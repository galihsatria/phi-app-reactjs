import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { LinearProgress, AppBar, Toolbar, Button, Paper, Typography, Grid, IconButton, Card, CardContent, Table, TableHead, TableBody, TableRow, TableCell, List, ListItem, ListItemText, ListItemSecondaryAction } from '@material-ui/core';
import { Mood, Dashboard, ChevronRight } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class HomeScreen extends React.Component {
    state = {
        isLoading: false,
        verifiedVendor: 0,
        uploadedVendor: 0,
        totalVendor: 0,
        rfqInProgress: 0,
        wfapproval: [],
        myapprovallist: [],
    }

    componentDidMount() {
        let authorizations = localStorage.getItem('userAuth');
        if(authorizations.includes('VENDORINPUT')) {
            this.setState({isLoading: true})

            let authToken = localStorage.getItem('authToken');
            axios.get(API_URL + '/myprofile', {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status = 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                    })
                    // mari redirect ke halaman khusus vendor input
                    this.props.history.push(BASE_URL + '/app/vendordetail/' + response.data.user.groupap);
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false,
                })
            })

            
        }

        if(authorizations.includes('DASHBOARD')) {
            this.setState({isLoading: true})

            let authToken = localStorage.getItem('authToken');
            axios.get(API_URL + '/dashboard', {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status = 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        verifiedVendor: response.data.verifiedvendor.total,
                        uploadedVendor: response.data.uploadedvendor.total,
                        totalVendor: response.data.totalvendor.total,
                        rfqInProgress: response.data.rfqinprogress.total,
                        wfapproval: response.data.wfapproval
                    })
                }
            }).catch((error) => {
                console.log(error);
                this.setState({
                    isLoading: false,
                })
            })
        }

        this.setState({isLoading: true})

        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + '/myapproval', {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    myapprovallist: response.data.myapproval
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }
    
    render() {
        const userName = localStorage.getItem('userName');
        let authorizations = localStorage.getItem('userAuth');
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <Dashboard /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">SELAMAT DATANG, {userName}</Typography>
                        
                    </Toolbar>
                </AppBar>
                {authorizations.includes('ESCM') && (

                <div>
                <Grid container>
                    <Grid item xs style={{margin: 10}}>
                        <Card>
                            <CardContent>
                                <Typography style={{textAlign: 'center'}} variant="display4" component="h4">{this.state.totalVendor}</Typography>
                                <Typography style={{textAlign: 'center'}} variant="subheading">Total Vendor</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs style={{margin: 10}}>
                        <Card>
                            <CardContent>
                                <Typography style={{textAlign: 'center'}} variant="display4" component="h4">{this.state.uploadedVendor}</Typography>
                                <Typography style={{textAlign: 'center'}} variant="subheading">Vendor dengan Dokumen</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs style={{margin: 10}}>
                        <Card>
                            <CardContent>
                            <Typography style={{textAlign: 'center'}} variant="display4" component="h4">{this.state.verifiedVendor}</Typography>
                                <Typography style={{textAlign: 'center'}} variant="subheading">Vendor Terverifikasi</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Typography variant="subheading" component="h3" style={{margin: 12, marginTop: 20}}>
                    Menunggu Persetujuan Saya
                </Typography>
                <Grid container>
                    <Grid item xs={12}>
                    <Paper>
                        <List>
                            {this.state.myapprovallist.map((wfappr, index) => (
                                <ListItem key={index}>
                                    <ListItemText>{wfappr.wfname}</ListItemText>
                                    <ListItemSecondaryAction>
                                        <IconButton component={Link} to={BASE_URL + "/app/inputpengadaan/" + wfappr.ownerid}>
                                            <ChevronRight />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            ))}
                        </List>
                    </Paper>
                    </Grid>
                </Grid>
                </div>
                )}
            </div>
        )
    }
}

export default withRouter(HomeScreen);