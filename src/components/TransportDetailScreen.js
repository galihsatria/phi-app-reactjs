import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Snackbar, AppBar, Toolbar, Typography, Paper, Grid, TextField, Chip, Avatar, Tooltip, 
    Dialog, DialogTitle, DialogContent,
    IconButton, Table, TableCell, TableBody, TableRow, TableHead, FormControl, InputLabel, Select, 
    OutlinedInput, MenuItem, LinearProgress } from '@material-ui/core';
import { DepartureBoard, Place, Check, ArrowForward, ChevronLeft, Assignment } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class TransportDetailScreen extends React.Component {
    state = {
        isLoading: false,
        showMessagebar: false,
        messagebar: '',

        booking: null,

        bookingid: '',
        bookingType: '',
        requestorName: '',
        status: '',
        origin: '',
        destination: '',
        departureTime: null,
        returnTime: null,
        remarks: '',
        passengers: [],
        transportRemarks: '',
        quantity: 0,

        assignedCar: '',
        assignedDriver: '',
        assignedVoucherQty: 0,
        vehicleList: [],
        driverList: [],

        isErrorAssignedCar: false,
        isErrorVoucherQty: false,
    }

    componentWillMount() {
        let booking = this.props.location.book
        let vehicleList = this.props.location.vehicleList
        let driverList = this.props.location.driverList

        if(booking != null) {
            this.setState({
                bookingid: booking.id,
                bookingType: booking.bookingtype,
                requestorName: booking.requestor.name,
                status: booking.wfstatus,
                origin: booking.origin,
                destination: booking.destination,
                departureTime: booking.departuredate,
                returnTime: booking.returndate,
                remarks: booking.remarks,
                passengers: booking.passengers,
                vehicleList: vehicleList,
                driverList: driverList,
                quantity: booking.quantity,

                assignedCar: booking.assignedcarid,
                assignedDriver: booking.assigneddriverid,
                transportRemarks: booking.transportremarks,
            })
        }
    }

    componentDidMount() {
        let booking = this.props.location.book
    }
    
    handleCompleteRequest() {
        // validate dulu
        var formError = false
        this.setState({
            isErrorAssignedCar: false,
            isErrorVoucherQty: false
        })

        if(this.state.bookingType == 'MOBIL' && this.state.assignedCar == '') { formError = true; this.setState({isErrorAssignedCar: true}) }
        else if(this.state.bookingType == 'VOUCHER' && this.state.assignedVoucherQty <= 0) { 
            formError = true 
            this.setState({isErrorVoucherQty: true})
        }

        console.log('Assigned: ' + this.state.isErrorAssignedCar)

        if(formError) {
            return
        }

        // submit data
        this.setState({isLoading: true})
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/completebook`, {
            bookingid: this.state.bookingid,
            origin: this.state.origin,
            destination: this.state.destination,
            departuredate: this.state.departureTime,
            returndate: this.state.returnTime,
            quantity: this.state.assignedVoucherQty,
            assignedcar: this.state.assignedCar,
            assigneddriver: this.state.assignedDriver,
            transportremarks: this.state.transportRemarks,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                let booking = response.data.booking
                this.setState({
                    isLoading: false,
                    
                    bookingid: booking.id,
                    bookingType: booking.bookingtype,
                    requestorName: booking.requestor.name,
                    status: booking.wfstatus,
                    origin: booking.origin,
                    destination: booking.destination,
                    departureTime: booking.departuredate,
                    returnTime: booking.returndate,
                    remarks: booking.remarks,
                    passengers: booking.passengers,
                    quantity: booking.quantity,

                    assignedCar: booking.assignedcarid,
                    assignedDriver: booking.assigneddriverid,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    render() {
        
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                {this.state.showMessagebar && (
                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={this.state.showMessagebar}
                        onClose={() => this.setState({showMessagebar: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.messagebar}</span>}
                    />
                )}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                        <DepartureBoard />
                        <Typography style={{ flexGrow: 1 }} variant="body1">BOOKING DETAIL</Typography>
                        <Tooltip title="Back">
                            <IconButton component={Link} to={BASE_URL + "/app/transport"} color="primary">
                                <ChevronLeft />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Assign and Complete">
                            <IconButton onClick={() => this.handleCompleteRequest()} color="primary">
                                <Assignment />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <Paper style={{padding: 20}}>
                    <Typography variant="h6">Request {this.state.bookingType}, oleh {this.state.requestorName}</Typography>
                    {this.state.status == 'ASSIGNED' ? (
                        <Chip
                            avatar={<Avatar><Check /></Avatar>}
                            color="primary"
                            label="ASSIGNED" />
                    ) : (
                        <Chip
                            avatar={<Avatar><ArrowForward /></Avatar>}
                            label={this.state.status} />
                    )}

                    <Grid container spacing={24} style={{marginTop: 10}}>
                        <Grid item xs={6}>
                            <TextField 
                                value={this.state.origin}
                                onChange={(e) => this.setState({origin: e.target.value})}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Asal Perjalanan" 
                                />
                            <TextField 
                                value={this.state.destination}
                                onChange={(e) => this.setState({destination: e.target.value})}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Tujuan" 
                                />
                            <TextField 
                                value={this.state.departureTime}
                                onChange={(e) => this.setState({departureTime: e.target.value})}
                                InputLabelProps={{shrink: true}}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Waktu Berangkat" 
                                />
                            
                            <TextField 
                                value={this.state.returnTime}
                                onChange={(e) => this.setState({returnTime: e.target.value})}
                                InputLabelProps={{shrink: true}}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Sampai Dengan" 
                                />
                            <TextField
                                value={this.state.remarks} 
                                onChange={(e) => this.setState({remarks: e.target.value})}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Catatan" 
                                />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                value={this.state.assignedCar} 
                                disabled
                                onClick={() => this.setState({vehicleDialog: true})}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Mobil yang Diberikan" 
                                />
                            <FormControl 
                                error={this.state.isErrorAssignedDriver}
                                fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel shrink htmlFor="outlined-driver">Driver</InputLabel>
                                <Select
                                    value={this.state.assignedDriver}
                                    onChange={(e) => this.setState({assignedDriver: e.target.value})}
                                    input={
                                    <OutlinedInput
                                        labelWidth={200}
                                        name="driver"
                                        id="outlined-driver"
                                    />
                                    }
                                >
                                {this.state.driverList.map((driver, index) => (
                                    <MenuItem key={index} value={driver.id}>{`${driver.name} - ${driver.phone}`}</MenuItem>
                                ))}
                                    
                                </Select>
                            </FormControl>
                            <TextField 
                                value={this.state.quantity}
                                onChange={(e) => this.setState({quantity: e.target.value})}
                                error={this.state.isErrorVoucherQty}
                                helperText={this.state.isErrorVoucherQty && 'Isian tidak valid'}
                                disabled={this.state.bookingType != 'VOUCHER'}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Jumlah" 
                                />
                            <TextField 
                                value={this.state.transportRemarks}
                                onChange={(e) => this.setState({transportRemarks: e.target.value})}
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="Catatan dari Transport" 
                                />
                        </Grid>
                    </Grid>
                </Paper>
                <Toolbar>
                    <Typography style={{ flexGrow: 1 }} variant="body1">DAFTAR PENUMPANG</Typography>
                </Toolbar>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nama</TableCell>
                                <TableCell>Department</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.passengers.map((p, index) => (
                                <TableRow>
                                    <TableCell>{p.name}</TableCell>
                                    <TableCell>{p.userdepartment.name}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <Dialog fullWidth={true} maxWidth="900" open={this.state.vehicleDialog} onClose={() => this.setState({vehicleDialog: false})}>
                    <DialogTitle>Mobil yang Tersedia</DialogTitle>
                    <DialogContent>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Mobil</TableCell>
                                    <TableCell>Asal</TableCell>
                                    <TableCell>Tujuan</TableCell>
                                    <TableCell>Mulai</TableCell>
                                    <TableCell>Selesai</TableCell>
                                    <TableCell>Jumlah Penumpang</TableCell>
                                </TableRow>
                            </TableHead>
                        </Table>
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default TransportDetailScreen;