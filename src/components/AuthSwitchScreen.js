import React from 'react';
import { Route, Redirect, Switch } from "react-router-dom";
import LoginScreen from './LoginScreen';
import ResponsiveDrawer from './ResponsiveDrawer';

import { BASE_URL } from '../config';

export class AuthSwitchScreen extends React.Component {
    state = {
        authToken: null,
    }

    componentDidMount() {
        let token = localStorage.getItem('authToken');
        if(token) {
            this.setState({
                authToken: token
            })   
        }
    }

    render() {
        let token = localStorage.getItem('authToken');
        return (
            <Switch>
                <Route exact path={BASE_URL + '/'} render={() => <div>{token == null ? <Redirect to={BASE_URL + '/login'} /> : <Redirect to={BASE_URL + '/app'} />}</div> } />
                <Route path={`${BASE_URL}/login`} component={LoginScreen} />
                <Route path={`${BASE_URL}/app`} component={ResponsiveDrawer} />
            {/*  */}
            </Switch>
        );
    }
}

export default AuthSwitchScreen;