import React from 'react';
import axios from 'axios';
import { Grid, Paper, Typography, TextField, Toolbar, Button, LinearProgress, Table, TableHead, TableBody, TableRow, TableCell } from '@material-ui/core';
import { API_URL, BASE_URL } from '../config';

export class BeritaAcaraScreen extends React.Component {
    state = {
        latarbelakang: '',
        maksudtujuan: '',
        ruanglingkup: '',
        prosesDetail: '',
        evaluasikomersial: '',
        negosiasi: '',
        nomorsurat: '',

        // bid summary
        proseskualifikasi: '',
        dasarhps: '',
        metodekualifikasi: '',
        klausapenalti: '',
        lainlain: '',

        bidderList: [],
        bideval: [],
        rfq: '',
        procmethod2: '',


        isLoading: false,
    };

    componentDidMount() {
        this.setState({isLoading: true})
        // get current
        const { rfqid } = this.props;
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/getbaep/${rfqid}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false, 
                    bidderList: response.data.bidderlist,          
                    rfq: response.data.rfq,   
                    procmethod2: response.data.procmethod2,
                    bideval: response.data.bideval,     
                })

                // mapping
                response.data.baeplist.forEach(element => {
                    var content = element.content
                    switch(element.section) {
                        case "LATARBELAKANG":
                            this.setState({latarbelakang: content})
                            break;
                        case "MAKSUDTUJUAN":
                            this.setState({maksudtujuan: content})
                            break;
                        case "RUANGLINGKUP":
                            this.setState({ruanglingkup: content})
                            break;
                        case "PROSESDETAIL":
                            this.setState({prosesDetail: content})
                            break;
                        case "EVALKOMERSIAL":
                            this.setState({evaluasikomersial: content})
                            break;
                        case "NEGOSIASI":
                            this.setState({negosiasi: content})
                            break;
                        case "NOMORSURAT":
                            this.setState({nomorsurat: content})
                            break;
                        case "KUALIFIKASI":
                            this.setState({proseskualifikasi: content})
                            break;
                        case "DASARHPS":
                            this.setState({dasarhps: content})
                            break;
                        case "METKUALIFIKASI":
                            this.setState({metodekualifikasi: content})
                            break;
                        case "PENALTI":
                            this.setState({klausapenalti: content})
                            break;
                        case "LAINLAIN":
                            this.setState({lainlain: content})
                            break;
                    }
                });
            }
            else {
                this.setState({
                    isLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }

    updateBaepBackend(section, order, content) {
        // update to backend
        const { rfqid } = this.props;
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/updatebaep`, {
            rfqid: rfqid,
            section: section,
            order: order,
            content: content
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,                   
                })
            }
            else {
                this.setState({
                    isLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }

    processNomorsurat = (e) => {
        var currentValue = e.target.value;
        this.setState({nomorsurat: currentValue})
        this.updateBaepBackend('NOMORSURAT', '1', currentValue)
    }

    processRuangLingkup = (e) => {
        var currentValue = e.target.value;
        this.setState({ruanglingkup: currentValue})
        this.updateBaepBackend('RUANGLINGKUP', '1', currentValue)
    }

    processMaksudTujuan = (e) => {
        var currentValue = e.target.value;
        this.setState({maksudtujuan: currentValue})
        this.updateBaepBackend('MAKSUDTUJUAN', '1', currentValue)
    }

    processNegosiasi = (e) => {
        var currentValue = e.target.value;
        this.setState({negosiasi: currentValue})
        this.updateBaepBackend('NEGOSIASI', '1', currentValue)
    }

    processLatarBelakang = (e) => {
        var currentValue = e.target.value;
        // currentValue = currentValue.replace('[rfqnum]' , this.props.rfqnum); <-- ini diproses di function tersendiri
        this.setState({
            latarbelakang: currentValue
        })

        this.updateBaepBackend('LATARBELAKANG', '1', currentValue)
    }

    processProsesDetail = (e) => {
        var currentValue = e.target.value;
        this.setState({prosesDetail: currentValue})
        this.updateBaepBackend('PROSESDETAIL', '1', currentValue)
    }

    processEvaluasiKomersial = (e) => {
        var currentValue = e.target.value;
        this.setState({evaluasikomersial: currentValue})
        this.updateBaepBackend('EVALKOMERSIAL', '1', currentValue)
    }

    processProsesKualifikasi = (e) => {
        var currentValue = e.target.value;
        this.setState({proseskualifikasi: currentValue})
        this.updateBaepBackend('KUALIFIKASI', '1', currentValue)
    }

    processDasarHPS = (e) => {
        var currentValue = e.target.value;
        this.setState({dasarhps: currentValue})
        this.updateBaepBackend('DASARHPS', '1', currentValue)
    }

    processMetodeKualifikasi = (e) => {
        var currentValue = e.target.value;
        this.setState({metodekualifikasi: currentValue})
        this.updateBaepBackend('METKUALIFIKASI', '1', currentValue)
    }

    processKlausaPenalti = (e) => {
        var currentValue = e.target.value;
        this.setState({klausapenalti: currentValue})
        this.updateBaepBackend('PENALTI', '1', currentValue)
    }

    processLainLain = (e) => {
        var currentValue = e.target.value;
        this.setState({lainlain: currentValue})
        this.updateBaepBackend('LAINLAIN', '1', currentValue)
    }

    getAmbilDokumenCount = () => {
        var counter = 0;
        this.state.bidderList.forEach(bidder => {
            if(bidder.ambildokumen === 1) {
                counter++;
            }
        });
        return counter;
    }

    render() {
        const { rfqnum } = this.props;

        return (
            <div>
            {this.state.isLoading && <LinearProgress />}
            <Grid container style={{padding: 20}}>
                <Grid item xs={6}>
                    <Typography variant="button">Bid Summary</Typography>
                    <TextField label="Penjelasan Singkat Proses Kualifikasi" margin="normal" multiline fullWidth value={this.state.proseskualifikasi} onChange={this.processProsesKualifikasi} />
                    <TextField label="Dasar Pembuatan HPS" margin="normal" multiline fullWidth value={this.state.dasarhps} onChange={this.processDasarHPS} />
                    <TextField label="Metode dan Kriteria Evaluasi" margin="normal" multiline fullWidth value={this.state.metodekualifikasi} onChange={this.processMetodeKualifikasi} />
                    <TextField label="Klausa Penalti" margin="normal" multiline fullWidth value={this.state.klausapenalti} onChange={this.processKlausaPenalti} />
                    <TextField label="Lain-Lain" margin="normal" multiline fullWidth value={this.state.lainlain} onChange={this.processLainLain} />

                    <br/>&nbsp; <br/> &nbsp;
                    <Typography variant="button">Berita Acara Evaluasi Penawaran (BAEP)</Typography>
                    <TextField label="Nomor Surat" margin="normal" multiline fullWidth value={this.state.nomorsurat} onChange={this.processNomorsurat} />
                    <TextField label="Latar Belakang" margin="normal" multiline fullWidth value={this.state.latarbelakang} onChange={this.processLatarBelakang} />
                    <TextField label="Maksud &amp; Tujuan" margin="normal" multiline fullWidth value={this.state.maksudtujuan} onChange={this.processMaksudTujuan} />
                    <TextField label="Ruang Lingkup" margin="normal" multiline fullWidth value={this.state.ruanglingkup} onChange={this.processRuangLingkup} />
                    <TextField label="Proses Detail Pengadaan" margin="normal" multiline fullWidth value={this.state.prosesDetail} onChange={this.processProsesDetail} />
                    <TextField label="Evaluasi Komersial" margin="normal" multiline fullWidth value={this.state.evaluasikomersial} onChange={this.processEvaluasiKomersial} />
                    <TextField label="Proses Negosiasi" margin="normal" multiline fullWidth value={this.state.negosiasi} onChange={this.processNegosiasi} />
                </Grid>
                <Grid item xs={6}>
                    <Paper style={{padding: 10}}>
                        <center><Typography variant="button">BERITA ACARA EVALUASI PENAWARAN <br/> (PREVIEW - Untuk Actual Result Silakan Run Report)</Typography></center>
                        <p><Typography variant="body1">NOMOR SURAT: <u> {this.state.nomorsurat} </u> </Typography></p>
                        <p><Typography variant="body1">A. PENDAHULUAN</Typography></p>
                        <p><Typography variant="body1">1. LATAR BELAKANG</Typography></p>
                        <p><Typography variant="caption"><u>{this.state.latarbelakang}</u></Typography></p>
                        <p><Typography variant="body1">2. MAKSUD &amp; TUJUAN</Typography></p>
                        <p><Typography variant="caption"><u>{this.state.maksudtujuan}</u></Typography></p>
                        <p><Typography variant="body1">3. RUANG  LINGKUP</Typography></p>
                        <p><Typography variant="caption"><u>{this.state.ruanglingkup}</u></Typography></p>
                        <p><Typography variant="caption">Jangka waktu pelaksanaan pekerjaan ini adalah untuk jangka waktu {this.state.rfq.periode} bulan</Typography></p>

                        <p><Typography variant="body1">B. PROSES DETAIL PENGADAAN</Typography></p>
                        <Typography variant="caption">
                        <p><u>
                            {this.state.prosesDetail}
                        </u></p>
                        <ol>
                        {this.state.bidderList.map((bidder, index) => (
                            <li key={index}><u>{bidder.vendorname}</u></li>
                        ))}
                        </ol>
                        <p>Justifikasi {this.state.procmethod2.description}:<br/>
                        {this.state.rfq.justifikasimetode}
                        </p>
                        <p>
                            Metode pemasukkan dokumen dengan menggunakan metode {this.state.rfq.sampul} sampul. 
                            Evaluasi yang dilakukan mengacu pada referensi persyaratan kriteria evaluasi teknis 
                            dan kriteria evaluasi komersial yang tercantum dalam dokumen pengadaan {this.state.rfq.rfqnum} 
                            yang telah diberikan kepada peserta pengadaan.
                        </p>

                        <p>
                        Surat undangan pengambilan dokumen pengadaan telah dikirimkan pada tanggal {this.state.rfq.invitationdate} kepada
                        seluruh peserta pengadaan tersebut di atas dengan keterangan sebagai berikut:
                        </p>

                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>No.</TableCell>
                                    <TableCell>Nama Perusahaan</TableCell>
                                    <TableCell>Kesimpulan</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.bidderList.map((bidder,index) => (
                                    <TableRow>
                                        <TableCell>{index+1}</TableCell>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell>{bidder.ambildokumen === 1 ? 'Mengambil dokumen pengadaan' : 'Tidak mengambil dokumen pengadaan'}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        <p>
                            Dari {this.state.bidderList.length} peserta pengadaan yang diundang, ada {this.getAmbilDokumenCount()} peserta pengadaan 
                            yang mengambil dokumen pengadaan pada tanggal {this.state.rfq.ambildokumendate}.
                        </p>
                        <p>
                            Rapat penjelasan (pre bid) {this.procmethod2} dilaksanakan pada tanggal {this.state.rfq.prebiddate} dan 
                            dihadiri oleh seluruh peserta pengadaan yang mengambil dokumen.
                        </p>
                        <p>
                        Waktu penyampaian dokumen penawaran dan pembukaan penawaran sampul 1 (administrasi dan teknikal) 
                        dilaksanakan pada tanggal {this.state.rfq.bidopeningdate} dengan hasil pembukaan penawaran adalah sebagai berikut:
                        </p>
                        
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>No.</TableCell>
                                    <TableCell>Nama Perusahaan</TableCell>
                                    <TableCell>Kelengkapan Dokumen Pada Saat Pembukaan Penawaran</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.bidderList.filter(bidder => bidder.ambildokumen === 1).map((bidder,index) => (
                                    <TableRow>
                                        <TableCell>{index+1}</TableCell>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell>{bidder.kelengkapandokumen === 1 ? 'Lengkap' : 'Tidak Lengkap'}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        <p>Dokumen penawaran peserta pengadaan selanjutnya direview dan dievaluasi lebih lanjut oleh tim evaluasi.</p>
                        </Typography>
                        <p><Typography variant="body1">C. HASIL EVALUASI</Typography></p>
                        <Typography variant="caption">
                        <p>Evaluasi yang dilakukan mengacu pada persyaratan kriteria evaluasi teknis dan kriteria evaluasi komersial 
                            yang tercantum dalam dokumen pengadaan {this.state.rfq.rfqnum} yang telah diberikan kepada peserta pengadaan:</p>
                        </Typography>
                        <p><Typography variant="body1">C1. Evaluasi Teknis  (Administrasi dan Teknikal)</Typography></p>
                        <Typography variant="caption">
                        <p>Metode evaluasi untuk aspek teknis (administrasi dan teknikal) adalah LULUS atau GAGAL. 
                            Berikut ini adalah ringkasan hasil evaluasi Teknis (dokumen administrasi dan dokumen teknis)</p>
                        </Typography>

                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama Perusahaan</TableCell>
                                    <TableCell>Hasil <br/> Evaluasi <br/> Administrasi</TableCell>
                                    <TableCell>Hasil <br/> Evaluasi <br/> Teknis</TableCell>
                                    <TableCell>Keterangan</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.bideval.map((bidder,index) => (
                                    <TableRow>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell>{bidder.evaladminresult == 1 ? 'Lulus' : 'Tidak Lulus'}</TableCell>
                                        <TableCell>{bidder.hasilteknis === 'LULUS' ? 'Lulus' : 'Tidak Lulus'}</TableCell>
                                        <TableCell>{bidder.keteranganteknis}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        <Typography variant="caption">
                        <p>Detail evaluasi untuk aspek teknis  (Administrasi dan Teknikal) terlampir.</p>
                        </Typography>

                        <p><Typography variant="body1">C2. Evaluasi Komersial</Typography></p>
                        <Typography variant="caption">
                        <p><u>{this.state.evaluasikomersial}</u></p>
                        <p>Metode evaluasi untuk aspek komersial adalah {this.state.rfq.sistemevaluasipenawaran}.</p>
                        <p>Setelah dilakukan evaluasi komersial, dokumen penawaran peserta pengadaan 
                        secara komersial dapat diterima dan selanjutnya dilakukan tahap negosiasi.</p>
                        <p>Berikut adalah perbandingan harga dengan HPS/OE </p>
                        <p>PHI Owner Estimate (OE): Rp. <b>{this.state.rfq.ownerestimate}</b></p>           
                        </Typography>

                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama Perusahaan</TableCell>
                                    <TableCell>Total Harga <br/> Penawaran <br/> (as bid)</TableCell>
                                    <TableCell>Variance <br/> to PHI OE</TableCell>
                                    <TableCell>Keterangan</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.bideval.filter(bidder => bidder.quoteprice > 0).map((bidder,index) => (
                                    <TableRow>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell>Rp. {bidder.quoteprice}</TableCell>
                                        <TableCell>{bidder.variance}%</TableCell>
                                        <TableCell>{bidder.keterangankomersial}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>

                        <Typography variant="caption">
                        <p>Detail evaluasi untuk aspek komersial terlampir.</p>
                        </Typography>

                        <p><Typography variant="body1">C3. Proses Negosiasi</Typography></p>
                        <Typography variant="caption">
                        <p><u>{this.state.negosiasi}</u></p>
                        <p>Berikut ini adalah ringkasan hasil evaluasi komersial setelah negosiasi:</p>
                        </Typography>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama Perusahaan</TableCell>
                                    <TableCell>Total Harga <br/> Penawaran <br/> (as bid)</TableCell>
                                    <TableCell>Harga Setelah Negosiasi</TableCell>
                                    <TableCell>Keterangan</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.bideval.filter(bidder => bidder.quoteprice > 0).map((bidder,index) => (
                                    <TableRow>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell>Rp. {bidder.quoteprice} <br/> ({bidder.variance}% terhadap OE)</TableCell>
                                        <TableCell>{bidder.afternegoprice > 0 && (
                                            <div>
                                                Rp. {bidder.afternegoprice} <br/>({bidder.negovariance}% terhadap OE)
                                            </div>
                                        )}</TableCell>
                                        <TableCell>{bidder.keterangankomersial}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        
                        <p><Typography variant="body1">D. REKOMENDASI</Typography></p>

                        <Typography variant="caption">
                        <p>Berdasarkan hasil evaluasi di atas, tim evaluator merekomendasikan &nbsp; 
                            {this.state.bideval.filter(bidder => bidder.winner == 1).map((bidder, index) => (
                                <span>{bidder.vendorname}</span>
                            ))} untuk ditunjuk sebagai pemenang pada pengadaan ini.
                        </p>
                        </Typography>
                        
                    </Paper>
                </Grid>
            </Grid>
            </div>
        )
    }
}

export default BeritaAcaraScreen;