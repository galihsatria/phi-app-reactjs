import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { Grid, TextField, Button, Snackbar, LinearProgress } from '@material-ui/core';

import { API_URL, BASE_URL } from '../config';

export class LoginScreen extends React.Component {
    state = {
        email: null,
        password: null,
        isEmailError: false,
        isError: false,
        errorMessage: null,
        isLoading: false,
    }

    handleLoginButton() {
        this.setState({
            isLoading: true,
        })
        // console.log(API_URL + '/login')
        axios.post(API_URL + '/login', {
            email: this.state.email,
            password: this.state.password
        })
            .then((response) => {
                this.setState({isLoading: false})
                if (response.data != null) {
                    if (response.data.status === 'GRANTED') {
                        if (response.data !== null && response.data.status === 'GRANTED') {
                            localStorage.setItem('authToken', response.data.token);
                            localStorage.setItem('userName', response.data.user.name);
                            localStorage.setItem('userEmail', response.data.user.email);
                            localStorage.setItem('userAuth', JSON.stringify(response.data.user.authorizations));
                            this.props.history.push(BASE_URL + '/');

                        }
                    }
                    else {
                        this.setState({
                            isError: true,
                            errorMessage: response.data.token
                        });
                    }
                }

            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    isLoading: false,
                    isError: true,
                    errorMessage: 'Error login'
                })
            });


    }

    validateEmail = (event) => {
        this.setState({ email: event.target.value })

    }

    render() {
        const { isError, isLoading } = this.state
        return (
            <div>
            {isLoading && <LinearProgress />}
            {isError && (
                <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={isError}
                        onClose={() => this.setState({isError: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.errorMessage}</span>}
                />
                )}
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ minHeight: 500 }}
            >
                <img alt="PERTAMINA" src="img/phsslogo.png" style={{ height: 70, margin: 15 }} />
                <TextField 
                    onChange={this.validateEmail} 
                    style={{ width: 300 }} 
                    label="Email" required 
                    id="email" 
                    margin="normal" 
                    variant="outlined" 
                    error={this.state.isEmailError}
                    helperText={this.state.isEmailError && 'Email tidak valid!'}
                    />
                <TextField onChange={(event) => { this.setState({ password: event.target.value }) }} type="password" style={{ width: 300 }} label="Password" required id="password" margin="normal" variant="outlined" />
                <Button onClick={() => this.handleLoginButton()} style={{ width: 300, height: 50, marginTop: 15 }} variant="contained" color="primary">Login</Button>
                Vendor Management System

            </Grid>
            </div>
        );
    }
}

export default withRouter(LoginScreen);