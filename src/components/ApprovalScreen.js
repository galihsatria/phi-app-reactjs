import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Typography, Tooltip, IconButton, LinearProgress, Table, TableHead, TableRow, TableCell, TableBody, Button } from '@material-ui/core';
import { ChevronLeft, DashboardRounded } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class ApprovalScreen extends React.Component {
    state = {
        isLoading: false,
        approval: []
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        this.setState({isLoading: true})

        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + '/approvaldetail/' + params.wfid, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    approval: response.data.approval
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }

    render() {
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <DashboardRounded /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">APPROVAL DETAIL</Typography>
                        <Tooltip title="Back">
                            <IconButton component={Link} to={BASE_URL + "/"} color="primary">
                                <ChevronLeft />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <br/>
                <Table>
                    <TableBody>
                        {!this.state.isLoading && this.state.approval.map((ap, i) => (
                            <TableRow>
                                <TableCell>{ap.name}</TableCell>
                                <TableCell>{ap.value}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <br/>
                <div style={{margin: 20}}>
                    <Button style={{marginBottom: 10}} fullWidth variant="contained" color="primary">APPROVE</Button>
                    <Button fullWidth variant="contained" color="secondary" fullWidth>REVISE</Button>
                </div>
            </div>
        )
    }
}

export default ApprovalScreen