import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { LinearProgress, Snackbar, AppBar, Toolbar, Typography, Paper, Tabs, Tab,
    Tooltip, IconButton, Table, TableHead, TableRow, TableCell, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, FormControl, InputLabel, Select, OutlinedInput, MenuItem, TableBody
} from '@material-ui/core';
import { Commute, AddCircle, Delete, DepartureBoard, Details, ChevronRight } from '@material-ui/icons';
import { API_URL, BASE_URL } from '../config';

export class TransportRequestScreen extends React.Component {
    state = {
        isLoading: false,
        showMessagebar: false,
        messagebar: '',

        vehicleDialog: false,
        deleteDialog: false,
        deleteDriverDialog: false,

        isNewCar: false,
        isNewDriver: false,
        isCarTypeError: false,
        isNopolError: false,
        isStatusError: false,
        isStatusError: false,

        isDriverEmailError: false,
        isDriverNameError: false,
        isDriverPhoneError: false,
        

        vehicle: null,
        vehicleList: [],
        driverList: [],
        deletedIndex: null,

        transportRequestList: [],

        tmpVehicleID: null,
        tmpCarType: '',
        tmpNomorPolisi: '',
        tmpCapacity: '',
        tmpVendor: '',
        tmpStatus: '',

        tmpDriverID: null,
        tmpDriverEmail: '',
        tmpDriverPhone: '',
        tmpDriverName: '',

        tabValue: 0,

    }

    handleDriverDelete = () => {
        var tmpDriverList = this.state.driverList
        let driver = this.state.driverList[this.state.deletedIndex]

        // delete yang di server
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/deletedriver/${driver.id}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                tmpDriverList.splice(this.state.deletedIndex, 1)
                this.setState({
                    isLoading: false,
                    deleteDriverDialog: false,
                    driverList: tmpDriverList,
                    showMessagebar: true,
                    messagebar: 'Berhasil menghapus data'
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleDelete = () => {
        var tmpVehicleList = this.state.vehicleList
        let vehicle = this.state.vehicleList[this.state.deletedIndex]

        // delete yang di server
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/deletemobil/${vehicle.id}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                tmpVehicleList.splice(this.state.deletedIndex, 1)
                this.setState({
                    isLoading: false,
                    deleteDialog: false,
                    vehicleList: tmpVehicleList,
                    showMessagebar: true,
                    messagebar: 'Berhasil menghapus data'
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })


        

    }

    openEditDriverDialog(index) {
        let driver = this.state.driverList[index]
        this.setState({
            isNewDriver: false,
            driverDialog: true,
            tmpDriverID: driver.id,
            tmpDriverEmail: driver.email,
            tmpDriverName: driver.name,
            tmpDriverPhone: driver.phone,
        })
    }

    openEditDialog(index) {
        let vehicle = this.state.vehicleList[index]
        this.setState({
            isNewCar: false,
            vehicleDialog: true,
            tmpVehicleID: vehicle.id,
            tmpCarType: vehicle.vehicletype,
            tmpNomorPolisi: vehicle.nomorpolisi,
            tmpCapacity: vehicle.capacity,
            tmpVendor: vehicle.vendor,
            tmpStatus: vehicle.status
        })
    }

    addNewDriver() {
        this.setState({
            driverDialog: true,
            isNewDriver: true,

            tmpDriverID: null,
            tmpDriverEmail: '',
            tmpDriverName: '',
            tmpDriverPhone: '',
        })
    }

    addNewCar = () => {
        this.setState({
            vehicleDialog: true, 
            isNewCar: true,
            tmpVehicleID: '',
            tmpCarType: '',
            tmpNomorPolisi: '',
            tmpCapacity: '',
            tmpVendor: '',
            tmpStatus: ''
        })
    }

    handleTabChange = (event, value) => {
        this.setState({tabValue: value})
    }

    handleUpdateDriver() {
        var formError = false;
        this.setState({
            isDriverEmailError: false,
            isDriverNameError: false,
            isDriverPhoneError: false,
        })

        if(this.state.tmpDriverEmail == '') {
            this.setState({isDriverEmailError: true})
            formError = true;
        } 
        if(this.state.tmpDriverName == '') {
            this.setState({isDriverNameError: true})
            formError = true;
        } 
        if(this.state.tmpDriverPhone == '') {
            this.setState({isDriverPhoneError: true})
            formError = true;
        } 
        if(formError) {
            return;
        }

        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/savedriver`, {
            driverid: this.state.tmpDriverID,
            name: this.state.tmpDriverName,
            email: this.state.tmpDriverEmail,
            phone: this.state.tmpDriverPhone,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var tmpDriverList = this.state.driverList
                let newDriver = response.data.driver
                if(this.state.isNewDriver) {
                    tmpDriverList.push(newDriver)
                }
                else {
                    let curindex = tmpDriverList.findIndex(driver => driver.id === this.state.tmpDriverID)
                    tmpDriverList[curindex] = newDriver
                }
                
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    driver: response.data.driver,
                    driverList: tmpDriverList,
                    driverDialog: false,                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleUpdateMobil = () => {
        var formError = false;
        this.setState({
            isCarTypeError: false,
            isNopolError: false,
            isCapacityError: false,
            isStatusError: false,
        })

        if(this.state.tmpCarType == '') {
            this.setState({isCarTypeError: true})
            formError = true;
        } 
        if(this.state.tmpNomorPolisi == '') {
            this.setState({isNopolError: true})
            formError = true;
        }
        if(this.state.tmpCapacity == '') {
            this.setState({isCapacityError: true})
            formError = true;
        }
        if(this.state.tmpStatus == '') {
            this.setState({isStatusError: true})
            formError = true;
        }
        if(formError) {
            return;
        }

        // upload
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/savemobil`, {
            vehicleid: this.state.tmpVehicleID,
            vehicletype: this.state.tmpCarType,
            nomorpolisi: this.state.tmpNomorPolisi,
            capacity: this.state.tmpCapacity,
            vendor: this.state.tmpVendor,
            status: this.state.tmpStatus
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var tmpVehicleList = this.state.vehicleList
                let newVehicle = response.data.vehicle
                if(this.state.isNewCar) {
                    tmpVehicleList.push(newVehicle)
                }
                else {
                    let curindex = tmpVehicleList.findIndex(vehicle => vehicle.id === this.state.tmpVehicleID)
                    tmpVehicleList[curindex] = newVehicle
                }
                
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    vehicle: response.data.vehicle,
                    vehicleList: tmpVehicleList,
                    vehicleDialog: false,                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    componentDidMount() {
        // get list of vehicles
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/getmobil`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    vehicleList: response.data.vehiclelist,
                    driverList: response.data.driverlist                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })

        // get transport request
        axios.get(API_URL + `/getbookingrequest`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                console.log(response.data.carrequest)
                this.setState({
                    isLoading: false,
                    transportRequestList: response.data.carrequest                    
                })
                
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    render() {
        return (
        <div>
            {this.state.isLoading && <LinearProgress />}
            <div style={{ height: 10, display: 'block' }} />
            {this.state.showMessagebar && (
                <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={this.state.showMessagebar}
                    onClose={() => this.setState({showMessagebar: false})}
                    autoHideDuration={6000}
                    message={<span>{this.state.messagebar}</span>}
                />
            )}
            <AppBar color="default" style={{marginTop: -10}} position="static">
                <Toolbar>
                <Commute /> &nbsp;
                    <Typography style={{ flexGrow: 1 }} variant="button">TRANSPORT MANAGEMENT</Typography>
                    
                </Toolbar>
            </AppBar>
            <Tabs value={this.state.tabValue} onChange={this.handleTabChange} >
                <Tab label="OUTSTANDING" />
                <Tab label="HISTORY" />
                <Tab label="MASTER DATA" />
            </Tabs>
            {this.state.tabValue === 0 && (
                <Paper style={{padding: 20}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Type</TableCell>
                                <TableCell>Requestor</TableCell>
                                <TableCell>Asal</TableCell>
                                <TableCell>Waktu</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell>Detail</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.transportRequestList.filter(book => book.status != 'COMPLETE').map((book, index) => (
                                <TableRow>
                                    <TableCell>{book.bookingtype}</TableCell>
                                    <TableCell>{book.requestor.name}</TableCell>
                                    <TableCell>{book.origin}</TableCell>
                                    <TableCell>{book.departuredate}</TableCell>
                                    <TableCell>{book.wfstatus}</TableCell>
                                    <TableCell>
                                        <IconButton 
                                            component={Link} 
                                            to={{
                                                pathname: `${BASE_URL}/app/bookingdetail/${book.id}`, 
                                                book: book, 
                                                driverList: this.state.driverList,
                                                vehicleList: this.state.vehicleList}}><ChevronRight /></IconButton>
                                        {/* <Link to={{pathname: `${BASE_URL}/app/bookingdetail/${book.id}`, book: book}}>Yha</Link> */}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            )}
            {this.state.tabValue === 1 && (
                <div>
                    <Paper style={{padding: 20}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Type</TableCell>
                                    <TableCell>Requestor</TableCell>
                                    <TableCell>Asal</TableCell>
                                    <TableCell>Waktu</TableCell>
                                    <TableCell>Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.transportRequestList.map((book, index) => (
                                    <TableRow>
                                        <TableCell>{book.bookingtype}</TableCell>
                                        <TableCell>{book.requestor.name}</TableCell>
                                        <TableCell>{book.origin}</TableCell>
                                        <TableCell>{book.departuredate}</TableCell>
                                        <TableCell>{book.wfstatus}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            )}
            {this.state.tabValue === 2 && (
                <div>
                <Toolbar>
                    <Typography style={{flexGrow: 1}} variant="body1">DATA MOBIL OPERASIONAL</Typography>
                    <Tooltip title="Tambah Mobil Baru">
                        <IconButton onClick={this.addNewCar} color="primary">
                            <AddCircle />
                        </IconButton>
                    </Tooltip>
                </Toolbar>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Type Mobil</TableCell>
                                <TableCell>Nomor Polisi</TableCell>
                                <TableCell>Status Operasi</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.vehicleList.map((car, index) => (
                                <TableRow>
                                    <TableCell><a onClick={() => this.openEditDialog(index)} style={{textDecoration: 'underline', cursor: 'pointer'}}>{car.vehicletype}</a></TableCell>
                                    <TableCell>{car.nomorpolisi}</TableCell>
                                    <TableCell>{car.status}</TableCell>
                                    <TableCell>
                                        <IconButton onClick={() => this.setState({deleteDialog: true, deletedIndex: index})}><Delete /></IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <Toolbar>
                    <Typography style={{flexGrow: 1}} variant="body1">DATA DRIVER</Typography>
                    <Tooltip title="Tambah Driver Baru">
                        <IconButton onClick={() => this.addNewDriver()} color="primary">
                            <AddCircle />
                        </IconButton>
                    </Tooltip>
                </Toolbar>
                <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nama</TableCell>
                                <TableCell>Phone</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.driverList.map((driver, index) => (
                                <TableRow>
                                    <TableCell><a onClick={() => this.openEditDriverDialog(index)} style={{textDecoration: 'underline', cursor: 'pointer'}}>{driver.name}</a></TableCell>
                                    <TableCell>{driver.phone}</TableCell>
                                    <TableCell>{driver.email}</TableCell>
                                    <TableCell>
                                        <IconButton onClick={() => this.setState({deleteDriverDialog: true, deletedIndex: index})}><Delete /></IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                </div>
            )}

            <Dialog open={this.state.driverDialog} onClose={() => this.setState({driverDialog: false})}>
                <DialogTitle>{this.state.isNewDriver ? 'Tambah Driver' : 'Edit Driver'}</DialogTitle>
                <DialogContent>
                    <TextField 
                        error={this.state.isDriverNameError}
                        helperText={this.state.isDriverNameError && "Nama Driver harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Nama Driver"
                        style={{marginTop: 20}}
                        value={this.state.tmpDriverName}
                        onChange={(e) => this.setState({tmpDriverName: e.target.value})} />
                    <TextField 
                        error={this.state.isDriverPhoneError}
                        helperText={this.state.isDriverPhoneError && "Telepon Driver harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Telepon"
                        style={{marginTop: 20}}
                        value={this.state.tmpDriverPhone}
                        onChange={(e) => this.setState({tmpDriverPhone: e.target.value})} />
                    <TextField 
                        error={this.state.isDriverEmailError}
                        helperText={this.state.isDriverEmailError && "Email Driver harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Email"
                        style={{marginTop: 20}}
                        value={this.state.tmpDriverEmail}
                        onChange={(e) => this.setState({tmpDriverEmail: e.target.value})} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({driverDialog: false})} color="primary">Cancel</Button>
                    <Button onClick={() => this.handleUpdateDriver()} color="primary">{this.state.isNewDriver ? 'Add' : 'Update'}</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={this.state.vehicleDialog} onClose={() => this.setState({vehicleDialog: false})}>
                <DialogTitle>{this.state.isNewCar ? 'Tambah Mobil' : 'Edit Mobil'}</DialogTitle>
                <DialogContent>
                    <TextField 
                        error={this.state.isCarTypeError}
                        helperText={this.state.isCarTypeError && "Type Mobil harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Type Mobil (Innova, Serena, dll)"
                        style={{marginTop: 20}}
                        value={this.state.tmpCarType}
                        onChange={(e) => this.setState({tmpCarType: e.target.value})} />
                    <TextField 
                        error={this.state.isNopolError}
                        helperText={this.state.isNopolError && "Nomor Polisi harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Nomor Polisi"
                        style={{marginTop: 20}}
                        value={this.state.tmpNomorPolisi}
                        onChange={(e) => this.setState({tmpNomorPolisi: e.target.value})} />
                    <TextField 
                        error={this.state.isCapacityError}
                        helperText={this.state.isCapacityError && "Kapasitas Mobil harus diisi"}
                        variant="outlined"
                        fullWidth
                        required
                        label="Kapasitas Mobil (Sopir tidak dihitung)"
                        style={{marginTop: 20}}
                        value={this.state.tmpCapacity}
                        onChange={(e) => this.setState({tmpCapacity: e.target.value})} /> 
                    <TextField 
                        variant="outlined"
                        fullWidth
                        label="Perusahaan Penyedia"
                        style={{marginTop: 20}}
                        value={this.state.tmpVendor}
                        onChange={(e) => this.setState({tmpVendor: e.target.value})} /> 
                    <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                        <InputLabel required htmlFor="status-operasi">Status Operasi</InputLabel>
                        <Select 
                            error={this.state.isStatusError}
                            helperText={this.state.isStatusError && "Status Operasional harus diisi"}
                            value={this.state.tmpStatus}
                            onChange={(e) => this.setState({tmpStatus: e.target.value})}
                            input={
                                <OutlinedInput labelWidth={100} name="statusoperasi"
                                    id="status-operasi" />
                            }
                        >
                            <MenuItem value="INACTIVE">INACTIVE</MenuItem>
                            <MenuItem value="ACTIVE">ACTIVE</MenuItem>
                        </Select>
                    </FormControl>       
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({vehicleDialog: false})} color="primary">Cancel</Button>
                    <Button onClick={this.handleUpdateMobil} color="primary">{this.state.isNewCar ? 'Add' : 'Update'}</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={this.state.deleteDriverDialog} onClose={() => this.setState({deleteDriverDialog: false})}>
                <DialogTitle>Konfirmasi Penghapusan</DialogTitle>
                <DialogContent>Anda yakin akan menghapus data ini?</DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({deleteDriverDialog: false})} color="primary">Cancel</Button>
                    <Button onClick={this.handleDriverDelete} color="primary">CONFIRM</Button>
                </DialogActions>
            </Dialog>
            <Dialog open={this.state.deleteDialog} onClose={() => this.setState({deleteDialog: false})}>
                <DialogTitle>Konfirmasi Penghapusan</DialogTitle>
                <DialogContent>Anda yakin akan menghapus data ini?</DialogContent>
                <DialogActions>
                    <Button onClick={() => this.setState({deleteDialog: false})} color="primary">Cancel</Button>
                    <Button onClick={this.handleDelete} color="primary">CONFIRM</Button>
                </DialogActions>
            </Dialog>
        </div>
        )
    }
}

export default TransportRequestScreen;