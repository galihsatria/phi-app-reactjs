import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { LinearProgress, AppBar, Toolbar, Typography, Button, Paper, Tooltip, IconButton, Table, TableHead, TableRow, TableCell, Grid, TextField, TableBody, Dialog, DialogTitle, DialogContent, DialogActions, Card, CardContent } from '@material-ui/core';
import { TrendingUp, ChevronLeft, ArrowUpward, ViewHeadline, CloudDownload } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class KpiDetailScreen extends React.Component {
    state = {
        isLoading: false,
        kpiheaderList: [],
        originalList: [],
        kpiAttachmentList: [],
        activity: null,
        activityDialog: false,
    }

    handleSearchByPeriod = (e) => {
        var tblist = this.state.originalList.filter((row) => { 
            return row.period.toUpperCase().indexOf(e.target.value.toUpperCase()) >= 0
        })

        this.setState({
            kpiheaderList: tblist
        })
    }

    handleSearchByAp = (e) => {
        var tblist = this.state.originalList.filter((row) => { 
            return row.apgroup.toUpperCase().indexOf(e.target.value.toUpperCase()) >= 0
        })

        this.setState({
            kpiheaderList: tblist
        })
    }

    openActivityDialog(kpi) {
        this.setState({
            activityDialog: true,
            activity: kpi
        })
    }

    openAttachmentDialog(kpiid) {
        this.setState({
            isDialogLoading: true,
            attachmentDialog: true,
            kpiAttachmentList: [],
            
        })

        let authToken = localStorage.getItem('authToken')
        axios.get(API_URL + `/kpiattachmentlist/${kpiid}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isDialogLoading: false,
                    kpiAttachmentList: response.data.attachmentlist,
                });
            }
            else {
                this.setState({isDialogLoading: false})
            }
        }).catch((error) => {
            this.setState({
                isDialogLoading: false,
            })
        })
    }

    componentDidMount() {
        this.setState({isLoading: true})
        let authToken = localStorage.getItem('authToken')
        axios.get(API_URL + `/kpidetail`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    kpiheaderList: response.data.kpiheaderlist,
                    originalList: response.data.kpiheaderlist,
                });
            }
            else {
                this.setState({isLoading: false})
            }
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }

    render() {
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <TrendingUp /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">DETAIL LAPORAN BULANAN</Typography>
                        <Tooltip title="Back">
                            <IconButton component={Link} to={BASE_URL + "/app/kpi"} color="primary">
                                <ChevronLeft />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <Paper style={{padding: 10}}>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-full-width"
                                label="Filter AP"
                                placeholder="PHI, PHM, PHSS, atau PHKT"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                onInput={this.handleSearchByAp}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="outlined-full-width"
                                label="Filter Periode"
                                placeholder="Periode Laporan"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                onInput={this.handleSearchByPeriod}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                    </Grid>
                </Paper>
                <br/>
                <Paper style={{padding: 10}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>AP</TableCell>
                                <TableCell>Periode</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell>Highlight</TableCell>
                                <TableCell>Aktivitas</TableCell>
                                <TableCell>Berkas Lampiran</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.kpiheaderList.map((kpi, index) => (
                                <TableRow>
                                    <TableCell>{kpi.apgroup}</TableCell>
                                    <TableCell>{kpi.period}</TableCell>
                                    <TableCell>{kpi.wfstatus}</TableCell>
                                    <TableCell>{kpi.highlight}</TableCell>
                                    <TableCell>
                                        <IconButton onClick={() => this.openActivityDialog(kpi)}>
                                            <ViewHeadline />
                                        </IconButton>
                                    </TableCell>
                                    <TableCell>
                                        <IconButton onClick={() => this.openAttachmentDialog(kpi.id)}>
                                            <ViewHeadline />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <Dialog fullWidth open={this.state.activityDialog} onClose={() => this.setState({activityDialog: false})}>
                    <DialogTitle>Aktivitas</DialogTitle>
                    <DialogContent>
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>Achievement</Typography>
                                {this.state.activity && this.state.activity.achievement && (<div>{this.state.activity.achievement.split('\n').map((item, key) => (
                                    <p>{item}</p>
                                ))}</div>)}
                            </CardContent>
                        </Card>
                        <br/>
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>Organization &amp; Personnel</Typography>
                                {this.state.activity && this.state.activity.organization && (<div>{this.state.activity.organization.split('\n').map((item, key) => (
                                    <p>{item}</p>
                                ))}</div>)}
                            </CardContent>
                        </Card>
                        <br/>
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>Event/Other Activity</Typography>
                                {this.state.activity && this.state.activity.events && (<div>{this.state.activity.events.split('\n').map((item, key) => (
                                    <p>{item}</p>
                                ))}</div>)}
                            </CardContent>
                        </Card>
                        <br/>
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>Highlight</Typography>
                                {this.state.activity && this.state.activity.highlight && (<div>{this.state.activity.highlight.split('\n').map((item, key) => (
                                    <p>{item}</p>
                                ))}</div>)}
                            </CardContent>
                        </Card>
                        <br/>
                        <Card>
                            <CardContent>
                                <Typography color="textSecondary" gutterBottom>Next Step/Correction Action Plan</Typography>
                                {this.state.activity && this.state.activity.nextactionplan && (<div>{this.state.activity.nextactionplan.split('\n').map((item, key) => (
                                    <p>{item}</p>
                                ))}</div>)}
                            </CardContent>
                        </Card>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({activityDialog: false})} color="primary">CLOSE</Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={this.state.attachmentDialog} onClose={() => this.setState({attachmentDialog: false})}>
                    {this.state.isDialogLoading && (<LinearProgress />)}
                    <DialogTitle>Berkas Lampiran</DialogTitle>
                    <DialogContent>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Document Name</TableCell>
                                    <TableCell>Download</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.kpiAttachmentList.map((att, index) => (
                                    <TableRow>
                                        <TableCell>{att.description}</TableCell>
                                        <TableCell>{att.docname}</TableCell>
                                        <TableCell>
                                            {att.docname && (
                                                <IconButton onClick={() => window.open(`${API_URL}/downloadattachment/${att.id}/KPI`, '_blank')}>
                                                    <CloudDownload />
                                                </IconButton>
                                            )}
                                            
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({attachmentDialog: false})} color="primary">CLOSE</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default KpiDetailScreen;