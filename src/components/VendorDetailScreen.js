import React from 'react';
import axios from 'axios';
import Downshift from 'downshift'
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { LinearProgress, Snackbar, AppBar, Toolbar, IconButton, Tooltip, Tab, Tabs, 
    Paper, Grid, TextField, Table, TableHead, TableRow, TableBody, TableCell,
    Dialog, DialogTitle, DialogContent, DialogActions, Button,
    FormControl, InputLabel, Select, OutlinedInput, MenuItem, TableFooter, Switch
} from '@material-ui/core';
import { LocalShipping, ChevronLeft, AttachFile, SaveAlt, ListAlt, Description, CloudDownloadOutlined, EditOutlined, DeleteOutline, CloudUploadOutlined, CloudUpload } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class VendorDetail extends React.Component {
    state = {
        isLoading: false,
        showMessagebar: false,
        tabValue: 0,
        isNewDocument: false,
        attachmentDialog: false,
        letterDialog: false,
        isErrorAttachmentName: false,
        isUploading: false,
        commodityDialog: false,

        disableAttachment: false,
        disableLetter: false,
        disableBidang: false,
        disableBidang: false,

        deletedCommodityIndex: '',
        deletedAttachmentIndex: '',
        deleteDialog: false,
        deleteAttachmentDialog: false,

        phivendorid: '',
        vendorid: '',
        name: '',
        address: '',
        city: '',
        district: '',
        npwp: '',
        telephone1: '',
        telephone2: '',
        faxnumber: '',
        email: '',
        bankname: '',
        bankaccountnumber: '',
        bankname2: '',
        bankaccountnumber2: '',
        golusaha: '',
        csmsscore: '',

        verified: false,
        verifieddate: '',

        // surat
        letterid: '',
        lettername: '',
        masaberlakuDate: '',
        nosurat: '',
        isErrorMasaberlakuDate: false,
        isErrorNoSurat: false,

        attachmentName: 'OTHER',

        commodity: null,

        commodityList: [],
        letterList: [],
        attachmentList: [],
        selectLetterList: [],
        selectAttachmentList: [],
        selectCommodityList: [],
        verifications: [],
    }

    uploadInput = null;

    uploadVerifications() {
        const { match: { params } } = this.props;
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/updateverificationlist', {
            vendorid: params.vendorid,
            verifications: this.state.verifications
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                })
            }
        }).catch((error) => {
            this.setState({
                isLoading: false,
            })
        })
    }

    updateVerificationRemarks(value, index) {
        var tmpVerifications = this.state.verifications
        var item = tmpVerifications[index]
        item.remarks = value
        tmpVerifications[index] = item
        this.setState({
            verifications: tmpVerifications
        })
        this.uploadVerifications()
    }

    toggleVerificationSwitch(index) {
        var tmpVerifications = this.state.verifications
        var item = tmpVerifications[index]
        item.result = item.result == '1' ? '0' : '1'
        tmpVerifications[index] = item
        
        this.setState({
            verifications: tmpVerifications
        })
        this.uploadVerifications()
    }

    componentDidMount() {
        const { match: { params } } = this.props;

        // khusus vendor yang input
        let authorizations = localStorage.getItem('userAuth');
        if(authorizations.includes('VENDORINPUT')) {
            this.setState({isLoading: true})

            let authToken = localStorage.getItem('authToken');
            axios.get(API_URL + '/myprofile', {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status = 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                    })
                    // mari redirect ke halaman khusus vendor input
                    if(response.data.user.groupap != params.vendorid)
                        this.props.history.push(BASE_URL + '/');
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false,
                })
            })

            
        }

        if(authorizations.includes('VENDORVERIFY')) {
            let authToken = localStorage.getItem('authToken')
            axios.get(API_URL + `/getvendorverification/${params.vendorid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status === 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        verifications: response.data.verifications,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })
        }

        if(params.vendorid != null) {
            this.setState({isLoading: true})
            let authToken = localStorage.getItem('authToken')
            axios.get(API_URL + `/vendordetail/${params.vendorid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status === 'SUCCESS') {
                    let vendor = response.data.vendor
                    console.log(vendor)
                    this.setState({
                        isLoading: false,
                        isNewDocument: false,

                        vendorid: vendor.vendorid != null ? vendor.vendorid : 'NA',
                        phivendorid: response.data.phivendorid,
                        name: vendor.name,
                        address: vendor.street,
                        city: vendor.city,
                        district: vendor.district,
                        email: vendor.email,
                        npwp: vendor.taxnumber,
                        telephone1: vendor.telephone1,
                        telephone2: vendor.telephone2,
                        faxnumber: vendor.faxnumber,
                        bankname: vendor.bankname,
                        bankaccountnumber: vendor.bankaccountno,
                        bankname2: vendor.bankname2,
                        bankaccountnumber2: vendor.bankaccountno2,
                        golusaha: vendor.golusaha,
                        csmsscore: vendor.csmsscore,

                        commodityList: (vendor.commodities != null) ? vendor.commodities : [],
                        letterList: (vendor.letters != null) ? vendor.letters : [],
                        attachmentList: (response.data.attachments != null) ? response.data.attachments : [],

                        disableAttachment: vendor.vendorid != null ? false : true,
                        disableBidang: vendor.vendorid != null ? false : true,
                        disableLetter: vendor.vendorid != null ? false : true,
                        disableBidang: vendor.vendorid != null ? false : true,

                        verified: response.data.verified,
                        verifieddate: response.data.verifieddate,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            axios.get(API_URL + `/getunavailableletter/${params.vendorid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status === 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        isNewDocument: false,

                        selectLetterList: response.data.letterlist,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            axios.get(API_URL + `/getattachmentlist`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status === 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        isNewDocument: false,

                        selectAttachmentList: response.data.attachmentlist,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            axios.get(API_URL + `/getcommoditylist`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status === 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        selectCommodityList: response.data.commoditylist,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })
        }
        else {
            this.setState({
                isNewDocument: true,
                disableAttachment: true,
                disableBidang: true,
                disableLetter: true,
                vendorid: 'NA',
            })
        }
    }

    handleTabChange = (event, value) => {
        this.setState({tabValue: value})
    }

    handleCommodityDialog = () => {
        this.setState({
            tabValue: 1,
            commodityDialog: true,
        })
    }

    

    handleLetterDialog = () => {
        this.setState({
            tabValue: 2,
            letterDialog: true,
            isNewDocument: true,
            letterid: '',
            letterName: '',
            nosurat: '',
            masaberlakuDate: ''
        });
    }

    handleAttachmentDialog = () => {
        this.setState({
            tabValue: 3,
            attachmentDialog: true,
        })
    }

    handleEditLetter = (index) => {
        this.setState({
            letterDialog: true,
            isNewDocument: false,
            isUploading: false,

            letterid: this.state.letterList[index].id,
            letterName: this.state.letterList[index].lettertype,
            nosurat: this.state.letterList[index].aktapendirianno,
            masaberlakuDate: this.state.letterList[index].sktvaliditydate
        })
        // this.handleSaveLetter();
    }

    handleSaveCommodity = () => {
        this.setState({
            isUploading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/savecommodity', {
            vendorid: this.state.vendorid,
            commid: this.state.commodity.commid,
            commdescription: this.state.commodity.description
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    isUploading: false,
                    messagebar: 'Dokumen berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    commodityDialog: false,

                    commodityList: response.data.commoditylist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen tidak berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    isUploading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Tidak dapat menyimpan dokumen',
                showMessagebar: true,
                isUploading: false,
            })
        })


    }

    handleSaveLetter = () => {
        if(this.state.letterName === '') {
            this.setState({
                isErrorAttachmentName: true,
            })
            return;
        }

        this.setState({
            isUploading: true,
            isErrorAttachmentName: false,
            isErrorMasaberlakuDate: false,
        })

        // upload! --- format tanggal
        let targetUrl = '/savenewletter'
        if(this.state.isNewDocument) {
            targetUrl = '/savenewletter'
        }
        else {
            targetUrl = '/updateletter'
        }
        
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + targetUrl, {
            letterid: this.state.letterid,
            vendorid: this.state.vendorid,
            lettertype: this.state.letterName,
            masaberlakudate: this.state.masaberlakuDate,
            nomorsurat: this.state.nosurat,          
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    isUploading: false,
                    messagebar: 'Dokumen berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    letterDialog: false,

                    letterList: response.data.letterlist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen tidak berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    isUploading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Tidak dapat menyimpan dokumen',
                showMessagebar: true,
                isUploading: false,
            })
        })
    }

    handleSave = () => {
        let authToken = localStorage.getItem('authToken');
        let flag = '';
        this.setState({isLoading: true})

        if(this.state.isNewDocument) {
            flag = 'NEWVENDOR';
        }
        else {
            flag = 'UPDATEVENDOR';
        }

        axios.post(API_URL + '/updatevendor', {
            flag: flag,
            vendorid: this.state.vendorid,
            phivendorid: this.state.phivendorid,
            name: this.state.name,
            address: this.state.address,
            city: this.state.city,
            district: this.state.district,
            npwp: this.state.npwp,
            telephone1: this.state.telephone1,
            telephone2: this.state.telephone2,
            faxnumber: this.state.faxnumber,
            email: this.state.email,
            bankname: this.state.bankname,
            bankaccountnumber: this.state.bankaccountnumber,
            bankname2: this.state.bankname2,
            bankaccountnumber2: this.state.bankaccountnumber2,
            golusaha: this.state.golusaha,   
            csmsscore: this.state.csmsscore,   
            
            verified: this.state.verified,
            verifieddate: this.state.verifieddate,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    vendorid: response.data.vendor.sapid,

                    disableAttachment: false,
                    disableBidang: false,
                    disableLetter: false,
                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen tidak berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Tidak dapat menyimpan dokumen',
                showMessagebar: true,
            })
        })
    }

    handleDeleteAttachment = () => {
        this.setState({
            isUploading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/deleteattachment', {
            attachmentid: this.state.attachmentList[this.state.deletedAttachmentIndex].id,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    isUploading: false,
                    messagebar: 'Data berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    deleteAttachmentDialog: false,

                    attachmentList: response.data.attachmentlist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Data tidak berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    isUploading: false,
                    deleteAttachmentDialog: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Error: Data tidak berhasil dihapus',
                showMessagebar: true,
                isUploading: false,
                deleteDialog: false,
            })
        })
    }

    handleDeleteCommodity = () => {
        this.setState({
            isUploading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/deletecommodity', {
            commodityid: this.state.commodityList[this.state.deletedCommodityIndex].id,
            vendorid: this.state.vendorid,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    isUploading: false,
                    messagebar: 'Data berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    deleteDialog: false,

                    commodityList: response.data.commoditylist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Data tidak berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    isUploading: false,
                    deleteDialog: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Error: Data tidak berhasil dihapus',
                showMessagebar: true,
                isUploading: false,
                deleteDialog: false,
            })
        })
    }

    handleUpload = (ev) => {
        ev.preventDefault();
        const { match: { params } } = this.props;

        this.setState({
            isErrorAttachmentName: false,
            isUploading: false,
        })

        if(this.state.attachmentName === '') {
            this.setState({
                isErrorAttachmentName: true
            })
            return;
        }

        const data = new FormData();
        data.append('file', this.uploadInput.files[0]);
        data.append('filename', this.state.attachmentName);
        data.append('vendorid', params.vendorid);

        let authToken = localStorage.getItem('authToken');
        this.setState({isUploading: true});
        axios.post(API_URL + '/uploadvendorfile', data, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then((response) => {
            // reload daftar lampiran
            if(response.data.status === 'SUCCESS') {
                console.log(response.data);
                this.setState({
                    attachmentList: response.data.doclist,
                    attachmentDialog: false,
                    isUploading: false,
                })
            }
            
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    render() {
        const { showMessagebar, tabValue, attachmentList, rowsPerPage, page  } = this.state;
        const userAuth = localStorage.getItem('userAuth');

        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                {this.state.showMessagebar && (
                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={showMessagebar}
                        onClose={() => this.setState({showMessagebar: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.messagebar}</span>}
                    />
                )}

                <div style={{ height: 10, display: 'block' }} />
                
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                        <LocalShipping />
                        <Typography style={{ flexGrow: 1 }} variant="body1">VENDOR DETAIL</Typography>
                        <Tooltip title="Back">
                            <IconButton component={Link} to={BASE_URL + "/app/vendormanagement"} color="primary">
                                <ChevronLeft />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Tambah Bidang/Subbidang">
                            <IconButton disabled={this.state.disableBidang} onClick={this.handleCommodityDialog} color="primary">
                                <ListAlt />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Tambah Surat-Surat">
                            <IconButton disabled={this.state.disableLetter} onClick={this.handleLetterDialog} color="primary">
                                <Description />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Tambah Lampiran">
                            <IconButton disabled={this.state.disableAttachment} onClick={this.handleAttachmentDialog} color="primary">
                                <AttachFile />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Save">
                        <IconButton onClick={this.handleSave} color="primary">
                            <SaveAlt />
                        </IconButton>
                    </Tooltip>
                    </Toolbar>
                </AppBar>

                <Tabs value={tabValue} onChange={this.handleTabChange} >
                    <Tab label="GENERAL DATA" />
                    <Tab label="BIDANG/SUBBIDANG" />
                    <Tab label="SURAT-SURAT" />
                    <Tab label="LAMPIRAN" />
                    {userAuth.includes('VENDORVERIFY') && <Tab label="VERIFICATION" />}
                </Tabs>
                {tabValue === 0 && (
                    <Paper style={{padding: 20}}>
                        <Grid container spacing={24}>
                            <Grid item xs={6}>
                                <TextField disabled value={this.state.vendorid} style={{marginTop: 12}} variant="outlined" fullWidth label="Vendor ID" />
                                <TextField onChange={(e) => {this.setState({name: e.target.value})}} value={this.state.name} style={{marginTop: 12}} variant="outlined" fullWidth label="Name" />
                                <TextField onChange={(e) => {this.setState({address: e.target.value})}} value={this.state.address} style={{marginTop: 12}} variant="outlined" fullWidth label="Address" />
                                <TextField onChange={(e) => {this.setState({city: e.target.value})}} value={this.state.city} style={{marginTop: 12}} variant="outlined" fullWidth label="City" />
                                <TextField onChange={(e) => {this.setState({district: e.target.value})}} value={this.state.district} style={{marginTop: 12}} variant="outlined" fullWidth label="District" />
                                <TextField onChange={(e) => {this.setState({bankname: e.target.value})}} value={this.state.bankname} style={{marginTop: 12}} variant="outlined" fullWidth label="Bank Name" />
                                <TextField onChange={(e) => {this.setState({bankname2: e.target.value})}} value={this.state.bankname2} style={{marginTop: 12}} variant="outlined" fullWidth label="Bank Name 2" />
                                <FormControl fullWidth style={{marginTop: 12, marginBottom: 12}} variant="outlined">
                                    <InputLabel htmlFor="outlined-golusaha">Golongan Usaha</InputLabel>
                                    <Select
                                        value={this.state.golusaha}
                                        onChange={(event) => {this.setState({golusaha: event.target.value})}}
                                        input={
                                        <OutlinedInput
                                            labelWidth={100}
                                            name="golusaha"
                                            id="outlined-golusaha"
                                            shrink={true}
                                        />
                                        }
                                    >
                                        <MenuItem key={1} value="B1">B1</MenuItem>
                                        <MenuItem key={2} value="B2">B2</MenuItem>
                                        <MenuItem key={3} value="Besar">Besar</MenuItem>
                                        <MenuItem key={4} value="M1">M1</MenuItem>
                                        <MenuItem key={5} value="M2">M2</MenuItem>
                                        <MenuItem key={6} value="Menengah">Menengah</MenuItem>
                                        <MenuItem key={7} value="K1">K1</MenuItem>
                                        <MenuItem key={8} value="K2">K2</MenuItem>
                                        <MenuItem key={9} value="K3">K3</MenuItem>
                                        <MenuItem key={10} value="Kecil">Kecil</MenuItem>
                                        <MenuItem key={1} value="N/A">N/A</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField onChange={(e) => {this.setState({npwp: e.target.value})}} value={this.state.npwp} style={{marginTop: 12}} variant="outlined" fullWidth label="NPWP" />
                                <TextField onChange={(e) => {this.setState({telephone1: e.target.value})}} value={this.state.telephone1} style={{marginTop: 12}} variant="outlined" fullWidth label="Telephone1" />
                                <TextField onChange={(e) => {this.setState({telephone2: e.target.value})}} value={this.state.telephone2} style={{marginTop: 12}} variant="outlined" fullWidth label="Telephone2" />
                                <TextField onChange={(e) => {this.setState({faxnumber: e.target.value})}} value={this.state.faxnumber} style={{marginTop: 12}} variant="outlined" fullWidth label="Fax Number" />
                                <TextField onChange={(e) => {this.setState({email: e.target.value})}} value={this.state.email} style={{marginTop: 12}} variant="outlined" fullWidth label="Email" />
                                <TextField onChange={(e) => {this.setState({bankaccountnumber: e.target.value})}} value={this.state.bankaccountnumber} style={{marginTop: 12}} variant="outlined" fullWidth label="Bank Account Number" />
                                <TextField onChange={(e) => {this.setState({bankaccountnumber2: e.target.value})}} value={this.state.bankaccountnumber2} style={{marginTop: 12}} variant="outlined" fullWidth label="Bank Account Number 2" />
                                <TextField onChange={(e) => {this.setState({csmsscore: e.target.value})}} value={this.state.csmsscore} style={{marginTop: 12}} variant="outlined" fullWidth label="CSMS Score" />
                            </Grid>
                        </Grid>
                    </Paper>
                )}
                {tabValue === 1 && (
                    <Paper style={{padding: 20}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Kode Bidang</TableCell>
                                    <TableCell>Bidang/Subbidang</TableCell>
                                    <TableCell>Gol Usaha</TableCell>
                                    <TableCell>Delete</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.commodityList.map((comm, index) => (
                                    <TableRow>
                                        <TableCell>{comm.commid}</TableCell>
                                        <TableCell>{comm.description}</TableCell>
                                        <TableCell>{comm.golusaha}</TableCell>
                                        <TableCell><IconButton onClick={() => this.setState({deletedCommodityIndex: index, deleteDialog: true})} ><DeleteOutline /></IconButton></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                )}
                {tabValue === 2 && (
                    <Paper style={{padding: 20}}>
                        
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Jenis Surat</TableCell>
                                    <TableCell>Nomor Surat</TableCell>
                                    <TableCell>Masa Berlaku</TableCell>
                                    <TableCell>Edit</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.letterList.map((letter, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{letter.description}</TableCell>
                                        <TableCell>{letter.aktapendirianno}</TableCell>
                                        <TableCell>{letter.sktvaliditydate}</TableCell>
                                        <TableCell><IconButton onClick={() => this.handleEditLetter(index)} ><EditOutlined /></IconButton></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                )}
                {tabValue === 3 && (
                    <Paper style={{padding: 20}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama Lampiran</TableCell>
                                    <TableCell>File</TableCell>
                                    <TableCell>Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.attachmentList.map((attachment, index) => (
                                    <TableRow>
                                        <TableCell>{attachment.category}</TableCell>
                                        <TableCell>{attachment.docname}</TableCell>
                                        <TableCell>
                                            {attachment.docname != null ?  (
                                            <div>
                                            <IconButton onClick={() => window.open(`${API_URL}/downloadattachment/${attachment.id}`, '_blank')} ><CloudDownloadOutlined /></IconButton>
                                            <IconButton onClick={() => this.setState({deletedAttachmentIndex: index, deleteAttachmentDialog: true})} ><DeleteOutline /></IconButton>
                                            </div>
                                            ) : (
                                            <IconButton onClick={() => this.setState({attachmentDialog: true, attachmentName: attachment.vlname}) }><CloudUpload /></IconButton>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                )}
                {tabValue === 4 && (
                    <div>
                        
                        <Paper>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Deskripsi</TableCell>
                                        <TableCell>Ada/Tidak Ada</TableCell>
                                        <TableCell width={300}>Keterangan</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                {this.state.verifications.map((vv, index) => (
                                    <TableRow>
                                        <TableCell>{vv.description}</TableCell>
                                        <TableCell>
                                            <Switch onChange={() => this.toggleVerificationSwitch(index)} checked={vv.result == '1'} color="primary" />
                                        </TableCell>
                                        <TableCell>
                                            <TextField onBlur={(e) => this.updateVerificationRemarks(e.target.value, index)} value={vv.remarks} variant="outlined" fullWidth />
                                        </TableCell>
                                    </TableRow>
                                ))}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TableCell>HASIL AKHIR</TableCell>
                                        <TableCell>
                                            <Switch onChange={() => {
                                                if(this.state.verified) {
                                                    this.setState({verifieddate: ''})
                                                }
                                                this.setState({verified: !this.state.verified})
                                            }} checked={this.state.verified == '1'} />
                                        </TableCell>
                                        <TableCell>
                                            <TextField onChange={(e) => this.setState({verifieddate: e.target.value})} InputLabelProps={{shrink: true}} value={this.state.verifieddate} type="date" label="Tanggal Verifikasi" variant="outlined" fullWidth />
                                        </TableCell>
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </Paper>
                    </div>
                )}

                <Dialog open={this.state.attachmentDialog} onClose={() => this.setState({attachmentDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <form onSubmit={this.handleUpload}>
                    <DialogTitle>Lampiran</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <FormControl fullWidth style={{marginTop: 12, marginBottom: 12}} variant="outlined">
                            <InputLabel htmlFor="outlined-attachmentname">Nama Lampiran</InputLabel>
                            <Select
                                value={this.state.attachmentName}
                                onChange={(event) => {this.setState({attachmentName: event.target.value})}}
                                input={
                                <OutlinedInput
                                    labelWidth={200}
                                    name="attachmentname"
                                    id="outlined-attachmentname"
                                />
                                }
                            >
                                {this.state.selectAttachmentList.map((attachment, index) => (
                                    <MenuItem key={index} value={attachment.name}>{attachment.description}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <input className="form-control"  ref={(ref) => { this.uploadInput = ref; }} type="file" />
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({attachmentDialog: false})} color="primary">
                        Cancel
                        </Button>
                        <Button type="submit" color="primary">
                            Upload
                        </Button>
                    </DialogActions>
                    </form>
                </Dialog>

                <Dialog open={this.state.letterDialog} onClose={() => this.setState({letterDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Surat-Surat</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        
                        <FormControl fullWidth style={{marginTop: 12, marginBottom: 12}} variant="outlined">
                        
                            <InputLabel htmlFor="outlined-lettername">Nama Surat</InputLabel>
                            <Select
                                InputLabelProps={{shrink: true}}
                                error={this.state.isErrorAttachmentName}
                                helperText="Isian tidak valid!"
                                value={this.state.letterName}
                                onChange={(event) => {this.setState({letterName: event.target.value})}}
                                input={
                                <OutlinedInput
                                    labelWidth={100}
                                    name="lettername"
                                    id="outlined-lettername"
                                />
                                
                                }
                            >
                            {this.state.selectLetterList.map((letter, index) => (
                                <MenuItem key={index} value={letter.name}>{letter.description}</MenuItem>
                            ))}

                            </Select>
                        </FormControl>
                        <TextField error={this.state.isErrorNoSurat} helperText={this.state.isErrorNoSurat && 'Isian tidak valid'} value={this.state.nosurat} onChange={(e) => {this.setState({nosurat: e.target.value})}} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} type="text" fullWidth label="No. Surat" />
                        <TextField error={this.state.isErrorMasaberlakuDate} helperText={this.state.isErrorMasaberlakuDate && 'Isian tidak valid'} value={this.state.masaberlakuDate} onChange={(e) => {this.setState({masaberlakuDate: e.target.value})}} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} type="date" fullWidth label="Masa Berlaku" required />
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({letterDialog: false, isUploading: false})} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={this.handleSaveLetter} type="submit" color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.commodityDialog} onClose={() => this.setState({commodityDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Bidang/Subbidang</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <Downshift
                            onChange={selection => this.setState({commodity: selection})}
                            itemToString={item => (item ? item.description : '')}
                        >
                            {({
                            getInputProps,
                            getItemProps,
                            getLabelProps,
                            getMenuProps,
                            isOpen,
                            inputValue,
                            highlightedIndex,
                            selectedItem,
                            }) => (
                            <div>
                                <TextField fullWidth variant="outlined" label="Cari Bidang/Subbidang" style={{marginTop: 12}} InputLabelProps={{shrink: true}} {...getInputProps()} />
                                <Paper {...getMenuProps()}>
                                {isOpen
                                    ? this.state.selectCommodityList
                                        .filter(item => item.commid.includes(inputValue.toUpperCase()))
                                        .map((item, index) => (
                                        <MenuItem
                                            {...getItemProps({
                                            key: index,
                                            index,
                                            item,
                                            })}
                                        >
                                           {item.commid} - {item.description}
                                        </MenuItem>
                                        ))
                                    : null}
                                </Paper>
                            </div>
                            )}
                        </Downshift>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({commodityDialog: false})} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={this.handleSaveCommodity} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.deleteDialog} onClose={() => this.setState({deleteDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Konfirmasi</DialogTitle>
                    <DialogContent>
                        <DialogContent>Anda akan menghapus entri ini. Apakah yakin?</DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({deleteDialog: false})} color="primary">
                        Batal
                        </Button>
                        <Button onClick={this.handleDeleteCommodity} color="primary">
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.deleteAttachmentDialog} onClose={() => this.setState({deleteAttachmentDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Konfirmasi</DialogTitle>
                    <DialogContent>
                        <DialogContent>Anda akan menghapus entri ini. Apakah yakin?</DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({deleteAttachmentDialog: false})} color="primary">
                        Batal
                        </Button>
                        <Button onClick={this.handleDeleteAttachment} color="primary">
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default VendorDetail;