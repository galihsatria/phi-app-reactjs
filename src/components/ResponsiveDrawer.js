import React from 'react';
import { Route, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';

import { withStyles } from '@material-ui/core/styles';
import { 
    AppBar, CssBaseline, Divider, Drawer, Hidden,
    Typography, Toolbar,
    IconButton, List, ListItem, ListItemIcon, ListItemText,
    Menu, MenuItem, Badge, DialogContentText, Dialog, DialogTitle, DialogContent,
    TextField, DialogActions, Button, LinearProgress, Snackbar
} from '@material-ui/core';
import { AccountCircle, CalendarToday, ExitToApp, Book, BookOutlined, MobileFriendly, LocalShipping, LocalShippingOutlined, Dashboard, DashboardOutlined, VpnKeyOutlined, People, PeopleOutlineOutlined, Commute, CommuteOutlined, TrendingUp, TrendingDown, TrendingUpOutlined, Room, RoomOutlined, MeetingRoom, MeetingRoomOutlined } from '@material-ui/icons';
import MenuIcon from '@material-ui/icons/Menu';
import MailIcon from '@material-ui/icons/Mail';

import BidderListScreen from './BidderListScreen';
import NewProcurementScreen from './NewProcurementScreen';
import VendorManagementScreen from './VendorManagementScreen';
import VendorDetailScreen from './VendorDetailScreen';
import HomeScreen from './HomeScreen';
import KpiInputScreen from './KpiInputScreen';
import KpiDetailScreen from './KpiDetailScreen';
import ApprovalScreen from './ApprovalScreen';

import { API_URL, BASE_URL } from '../config';
import TransportRequestScreen from './TransportRequestScreen';
import TransportDetailScreen from './TransportDetailScreen';
import KpiScreen from './KpiScreen';
import { MeetingRoomScreen } from './MeetingRoomScreen';

class ResponsiveDrawer extends React.Component {
    state = {
        isLoading: false,
        showMessagebar: false,
        mobileOpen: false,
        anchorEl: null,
        dialogOpen: false,
        changePasswordDialog: false,
        activemenu: 'app',
        messagebar: '',

        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
        isErrorCurrentPassword: false,
        isErrorConfirmPassword: false,
    };

    handleLogout = () => {
        localStorage.clear();
        this.props.history.push(BASE_URL + '/login');
    }

    handleProfileMenu = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleProfileMenuClose = () => {
        this.setState({ anchorEl: null });
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleDialogOpen = () => {
        this.setState({dialogOpen: true, anchorEl: null});
    }

    handleChangePasswordDialog = () => {
        this.setState({changePasswordDialog: true, anchorEl: null});
    }

    handleDialogClose = () => {
        this.setState({dialogOpen: false, changePasswordDialog: false})
    }

    handleSaveNewPassword = () => {
        this.setState({
            isErrorCurrentPassword: false,
            isErrorNewPassword: false,
            isErrorConfirmPassword: false,
        })

        // validasi
        if(this.state.currentPassword === '') {
            this.setState({isErrorCurrentPassword: true})
            return;
        }
        if(this.state.newPassword === '') {
            this.setState({isErrorNewPassword: true});
            return;
        }
        if(this.state.confirmPassword != this.state.newPassword) {
            this.setState({isErrorConfirmPassword: true})
            return;
        }

        // send to server
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/changepassword', {
            password: this.state.currentPassword,
            newpassword: this.state.newPassword,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    messagebar: response.data.message,
                    showMessagebar: true,
                    changePasswordDialog: false,
                })

                setTimeout(() => {
                    this.handleLogout();
                }, 2000)
                
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: response.data.message,
                    showMessagebar: true,
                    isNewDocument: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Cannot update!',
                showMessagebar: true,
            })
        })
    }

    componentDidMount() {
        document.title = 'PHSS VENDOR MANAGEMENT SYSTEM';
        let token = localStorage.getItem('authToken');
        if(token === null) {
            this.props.history.push(BASE_URL + '/login');
        }
    }

    render() {
        const { classes, theme } = this.props;
        const { anchorEl, activemenu } = this.state;

        const username = localStorage.getItem('userName');
        const userAuth = localStorage.getItem('userAuth');

        var listmenu = [];
        if(userAuth !== null) {
            listmenu = [{
                name: 'Home',
                path: 'app',
                icon: (activemenu == 'app') ? <Dashboard /> : <DashboardOutlined />,
                component: HomeScreen,
                authorized: 1,
            }, {
                name: 'Vendor Management',
                path: 'vendormanagement',
                icon: (activemenu == 'vendormanagement') ? <People /> : <PeopleOutlineOutlined />,
                component: VendorManagementScreen,
                authorized: userAuth.includes('VENDORMANAGEMENT'),
            }, {
                name: 'E-SCM',
                path: 'escm',
                icon: (activemenu == 'escm') ? <Book /> : <BookOutlined />,
                component: BidderListScreen,
                authorized: userAuth.includes('ESCM'),
            }, {
                name: 'KPI Online',
                path: 'kpi',
                icon: (activemenu == 'kpi') ? <TrendingUp /> : <TrendingUpOutlined />,
                component: KpiScreen,
                authorized: userAuth.includes('KPI')
            }, {
                name: 'Transport Management',
                path: 'transport',
                icon: (activemenu == 'transport') ? <Commute /> : <CommuteOutlined />,
                component: TransportRequestScreen,
                authorized: userAuth.includes('TRANSPORT')
            }, {
                name: 'Meeting Room Management',
                path: 'meetingroom',
                icon: (activemenu == 'meetingroom') ? <MeetingRoom /> : <MeetingRoomOutlined />,
                component: MeetingRoomScreen,
                authorized: userAuth.includes('MEETINGROOM')
            }
            ];
        }

        const drawer = (
            <div>
                {this.state.showMessagebar && (
                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={this.state.showMessagebar}
                        onClose={() => this.setState({showMessagebar: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.messagebar}</span>}
                    />
                )}
                <center>
                    <Link to={BASE_URL + "/"}>
                    <img alt="PHI LOGO" src={BASE_URL + "/img/phsslogo.png"} style={{ height: 50, marginTop: 10 }} />
                    </Link>
                </center>
                <div style={{ display: 'block', height: 15 }} />
                <Divider />
                <List>
                    {listmenu.map((menu, index) => (
                        <div>
                        {menu.authorized && (
                        <ListItem onClick={() => this.setState({activemenu: menu.path})} button component={Link} to={`${BASE_URL}/app/${menu.path}`} key={menu.path}>
                            <ListItemIcon>{menu.icon}</ListItemIcon>
                            <ListItemText primary={menu.name} />
                        </ListItem>
                        )}
                        {menu.path === 'kpi' && <Divider />}
                        </div>
                    ))}
                </List>
                <Divider/>
            </div>
        );

        return (
            <div className={classes.root}>
                    <CssBaseline />
                    <AppBar position="fixed" className={classes.appBar}>
                        <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="Open drawer"
                                onClick={this.handleDrawerToggle}
                                className={classes.menuButton}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="subtitle1" color="inherit" noWrap style={{ flexGrow: 1 }}>
                                PHSS VENDOR MANAGEMENT SYSTEM
                        </Typography>
                            {/* <IconButton color="inherit">
                                <Badge badgeContent={4} color="secondary">
                                    <MailIcon />
                                </Badge>
                            </IconButton> */}

                            <IconButton 
                                aria-haspopup="true" 
                                aria-owns={anchorEl ? 'menu-list-grow' : undefined} 
                                onClick={this.handleProfileMenu} 
                                color="inherit">
                                <AccountCircle />
                            </IconButton>
                            <Menu 
                                id="profile-menu" 
                                onClose={this.handleProfileMenuClose}
                                anchorEl={anchorEl} 
                                open={Boolean(anchorEl)}>
                                <MenuItem onClick={this.handleDialogOpen}><AccountCircle />&nbsp; Profile</MenuItem>
                                <MenuItem onClick={this.handleChangePasswordDialog}><VpnKeyOutlined />&nbsp; Change Password</MenuItem>
                                <MenuItem onClick={this.handleLogout}><ExitToApp />&nbsp; Logout</MenuItem>
                            </Menu>
                        </Toolbar>
                    </AppBar>
                    <nav className={classes.drawer}>
                        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                        <Hidden smUp implementation="css">
                            <Drawer
                                variant="temporary"
                                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                                open={this.state.mobileOpen}
                                onClose={this.handleDrawerToggle}
                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                        <Hidden xsDown implementation="css">
                            <Drawer
                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                                variant="permanent"
                                open
                            >
                                {drawer}
                            </Drawer>
                        </Hidden>
                    </nav>
                    <main className={classes.content}>
                        <div className={classes.toolbar} />
                        {listmenu.map((menu, index) => (
                            <Route key={index} path={`${BASE_URL}/app/${menu.path}`} exact component={menu.component} />
                        ))}
                        <Route key={110} path={`${BASE_URL}/app`} exact component={HomeScreen} />
                        <Route key={100} path={`${BASE_URL}/app/inputpengadaan`} exact component={NewProcurementScreen} />
                        <Route key={200} path={`${BASE_URL}/app/inputpengadaan/:rfqid`} exact component={NewProcurementScreen} />
                        <Route key={300} path={`${BASE_URL}/app/vendordetail/:vendorid`} exact component={VendorDetailScreen} />
                        <Route key={400} path={`${BASE_URL}/app/newvendor`} exact component={VendorDetailScreen} />
                        <Route key={500} path={`${BASE_URL}/app/BIDDERLIST/:rfqid`} exact component={NewProcurementScreen} />
                        <Route key={600} path={`${BASE_URL}/app/bookingdetail/:bookingid`} exact component={TransportDetailScreen} />
                        <Route key={700} path={`${BASE_URL}/app/kpiinput/:kpiperiod`} exact component={KpiInputScreen} />
                        <Route key={800} path={`${BASE_URL}/app/kpidetail`} exact component={KpiDetailScreen} />
                        <Route key={900} path={`${BASE_URL}/app/approvalscreen/:wfid`} exact component={ApprovalScreen} />
                    </main>
                    <Dialog open={this.state.dialogOpen} onClose={this.handleDialogClose}>
                        <DialogTitle>Profile</DialogTitle>
                        <DialogContent>
                            <DialogContentText>Name: {username}</DialogContentText>
                        </DialogContent>
                    </Dialog>

                    <Dialog open={this.state.changePasswordDialog} onClose={this.handleDialogClose}>
                        {this.state.isLoading && <LinearProgress />}
                        <DialogTitle>Change Password</DialogTitle>
                        <DialogContent>
                            <TextField type="password" helperText={this.state.isErrorCurrentPassword && 'Field is required'} error={this.state.isErrorCurrentPassword} value={this.state.currentPassword} onChange={(e) => {this.setState({currentPassword: e.target.value})}} autoFocus margin="dense" label="Current Password" fullWidth variant="outlined" />
                            <TextField type="password" helperText={this.state.isErrorCurrentPassword && 'Field is required'} error={this.state.isErrorNewPassword} value={this.state.newPassword} onChange={(e) => {this.setState({newPassword: e.target.value})}} margin="dense" label="New Password" fullWidth variant="outlined" />
                            <TextField type="password" helperText={this.state.isErrorConfirmPassword && 'Must be same with new password'} error={this.state.isErrorConfirmPassword} value={this.state.confirmPassword} onChange={(e) => {this.setState({confirmPassword: e.target.value})}} margin="dense" label="Password Confirmation" fullWidth variant="outlined" />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={() => this.setState({changePasswordDialog: false})} color="primary">
                            Cancel
                            </Button>
                            <Button onClick={this.handleSaveNewPassword} color="primary">
                                OK
                            </Button>
                        </DialogActions>
                    </Dialog>

            </div>
        );
    }
}

const drawerWidth = 200;
const styles = theme => ({
    root: {
        display: 'flex',
        textAlign: 'left',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: 0
    },
});

ResponsiveDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    // Injected by the documentation to work in an iframe.
    // You won't need it on your project.
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(ResponsiveDrawer);
