import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { DialogContent, LinearProgress, AppBar, Toolbar, Button, Paper, Typography, Grid, IconButton, Card, CardContent, Table, TableHead, TableBody, TableRow, TableCell, Tooltip, Snackbar, TextField, Dialog, DialogTitle, DialogActions } from '@material-ui/core';
import { Mood, Dashboard, ChevronRight, TrendingUp, Save, AttachFile, CloudUpload, Launch, ChevronLeft, Delete } from '@material-ui/icons';

import { API_URL, BASE_URL } from '../config';

export class KpiInputScreen extends React.Component {
    state = {
        isLoading: false,
        isUploading: false,
        attachmentDialog: false,
        uncompleteDataDialog: false,
        confirmationDialog: false,
        showMessagebar: false,
        messagebar: '',
        isNewData: true,

        period: '',
        status: '',
        uploadAttachmentName: '',
        uploadAttachmentDescription: '',

        kpilist: [],
        attachments: [],

        achievement: '',
        organization: '',
        events: '',
        highlight: '',
        nextactionplan: ''
    }

    uploadInput = null;

    componentDidMount() {
        const { match: { params } } = this.props;

        // get list of kpi
        this.setState({period: params.kpiperiod, isLoading: true})

        let authToken = localStorage.getItem('authToken')
        axios.get(API_URL + `/kpilist/${params.kpiperiod}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                let kpiheader = response.data.kpiheader
                let status = kpiheader.wfstatus
                this.setState({
                    isLoading: false,
                    kpilist: response.data.kpilist,
                    attachments: response.data.attachmentlist,
                    status: kpiheader.wfstatus,

                    achievement: kpiheader.achievement,
                    organization: kpiheader.organization,
                    events: kpiheader.events,
                    highlight: kpiheader.highlight,
                    nextactionplan: kpiheader.nextactionplan,

                    isNewData: status == 'DRAFT'
                })
                
                var tmpkpiValue = []
                var i = 0;
                for(i = 0; i < response.data.kpilist.length; i++) {
                    tmpkpiValue[i] = 0
                }
                this.setState({kpivalue: tmpkpiValue})
            }
            else {
                this.setState({isLoading: false})
            }
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }

    deleteDocument(docid, index) {
        let tmpAttachments = this.state.attachments
        tmpAttachments[index].docname = null
        this.setState({attachments: tmpAttachments})

        // delete
        let authToken = localStorage.getItem('authToken')
        this.setState({isLoading: true});
        axios.get(API_URL + '/kpideletefile/' + docid, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then((response) => {
            // reload daftar lampiran
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                })
            }
        })
        .catch((error) => {
            this.setState({isLoading: false})
            console.log(error);
        });
    }

    handleUpload(ev) {
        ev.preventDefault();

        const data = new FormData();
        data.append('period', this.state.period)
        data.append('name', this.state.uploadAttachmentName)
        data.append('file', this.uploadInput.files[0])

        let authToken = localStorage.getItem('authToken')
        this.setState({isUploading: true});
        axios.post(API_URL + '/uploadkpifile', data, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then((response) => {
            // reload daftar lampiran
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isUploading: false,
                    attachmentDialog: false,
                    attachments: response.data.doclist
                })
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleSave() {
        var formError = false
        // for (let i = 0; i < this.state.kpilist.length; i++) {
        //     const value = this.state.kpilist[i].value;
        //     if(value <= 0) {
        //         formError = true
        //         break
        //     }

        //     if(isNaN(value)) {
        //         formError = true
        //         break
        //     }
        // }
        if(this.state.achievement == '') formError = true
        if(this.state.organization == '') formError = true
        if(this.state.events == '') formError = true
        if(this.state.highlight == '') formError = true
        if(this.state.nextactionplan == '') formError = true

        for (let i = 0; i < this.state.attachments.length; i++) {
            const attachment = this.state.attachments[i];
            if(attachment.docname == null) {
                formError = true
                break
            }
        }

        if(formError) {
            this.setState({confirmationDialog: false, uncompleteDataDialog: true})
            return
        }

        // change status on the server
        this.setState({isLoading: true})
        let authToken = localStorage.getItem('authToken')
        axios.post(API_URL + `/kpisubmit`, {
            period: this.state.period,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    status: 'SUBMITTED',
                    confirmationDialog: false,
                    isNewData: false,
                    messagebar: 'Berhasil Mengirimkan Data',
                    showMessagebar: true
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    confirmationDialog: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                confirmationDialog: false,
                isLoading: false,
                messagebar: 'Tidak dapat submit data',
                showMessagebar: true,
            })
        })
    }

    updateKpiHeader(category, value) {
        // this.setState({isLoading: true})

        let authToken = localStorage.getItem('authToken')
        axios.post(API_URL + `/kpiheaderupdate`, {
            period: this.state.period,
            category: category,
            value: value
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            this.setState({
                isLoading: false,
            })
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Tidak dapat menyimpan dokumen',
                showMessagebar: true,
            })
        })

    }

    updateKpiState(value, index) {
        if(isNaN(value)) {
            var tmpkpiValue = this.state.kpilist
            tmpkpiValue[index].value = 0
        }
        else {
            var tmpkpiValue = this.state.kpilist
            tmpkpiValue[index].value = value
        }
        
        this.setState({
            kpilist: tmpkpiValue,
        })
    }

    updateKpi(kpiid, value, index) {
        // this.setState({
        //     isLoading: true
        // })

        // update backend hehehe
        let authToken = localStorage.getItem('authToken')
        axios.post(API_URL + `/kpiupdate`, {
            period: this.state.period,
            kpiid: kpiid,
            value: value
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            this.setState({
                isLoading: false,
            })
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }
    
    render() {
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                {this.state.showMessagebar && (
                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={this.state.showMessagebar}
                        onClose={() => this.setState({showMessagebar: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.messagebar}</span>}
                    />
                )}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <TrendingUp /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">INPUT KPI</Typography>
                        <Tooltip title="Back">
                            <IconButton component={Link} to={BASE_URL + "/app/kpi"} color="primary">
                                <ChevronLeft />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Submit and Complete">
                        <IconButton onClick={() => this.setState({confirmationDialog: true})} color="primary">
                            <Launch />
                        </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <Paper style={{padding: 20}}>
                    <Typography variant="button">PERIOD &amp; STATUS</Typography>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <TextField disabled value={this.state.period} margin="normal" variant="filled" fullWidth label="Period" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField disabled value={this.state.status} margin="normal" variant="filled" fullWidth label="Status" />
                        </Grid>
                    </Grid>
                </Paper>
                <br />
                <Paper style={{padding: 20}}>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <Typography variant="button">SCM INDICATORS</Typography>
                            {this.state.kpilist.map((kpi, index) => (
                                <TextField
                                    disabled={!this.state.isNewData}
                                    onChange={(e) => this.updateKpiState(e.target.value, index)} 
                                    onBlur={(e) => this.updateKpi(kpi.id, e.target.value, index)} 
                                    value={kpi.value} 
                                    margin="normal"
                                    InputLabelProps={{
                                        shrink: true,
                                      }}
                                    variant="filled" fullWidth label={kpi.description} />    
                            ))}
                            
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant="button">ATTACHMENTS</Typography>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Document Name</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.attachments.map((a, i) => (
                                        <TableRow>
                                            <TableCell>{a.description}</TableCell>
                                            <TableCell>
                                                {a.docname != null ? (
                                                    <div>
                                                    {a.docname}
                                                    </div>
                                                ) : (
                                                    <IconButton onClick={() => 
                                                        this.setState({
                                                            uploadAttachmentName: a.name, 
                                                            uploadAttachmentDescription: a.description,
                                                            attachmentDialog: true})}>
                                                        <CloudUpload />
                                                    </IconButton>
                                                )}
                                                
                                            </TableCell>
                                            <TableCell>
                                                        {this.state.status == 'DRAFT' && (
                                                            <IconButton onClick={() => this.deleteDocument(a.id, i)}>
                                                                <Delete />
                                                            </IconButton>
                                                        )}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Paper>
                <br />
                <Paper style={{padding: 20}}>
                    <TextField multiline rowsMax="6"
                        disabled={!this.state.isNewData}
                        margin="normal"
                        variant="filled"
                        value={this.state.achievement}
                        onChange={(e) => this.setState({achievement: e.target.value})}
                        onBlur={(e) => this.updateKpiHeader('achievement', e.target.value)}
                        InputLabelProps={{
                            shrink: true,
                          }}
                        label="ACHIEVEMENT"
                        fullWidth />  
                    <TextField multiline rowsMax="6"
                        disabled={!this.state.isNewData}
                        margin="normal"
                        variant="filled"
                        value={this.state.organization}
                        onChange={(e) => this.setState({ organization: e.target.value})}
                        onBlur={(e) => this.updateKpiHeader('organization', e.target.value)}
                        InputLabelProps={{
                            shrink: true,
                          }}
                        label="ORGANIZATION &amp; PERSONNEL"
                        fullWidth />     
                    <TextField multiline rowsMax="6"
                        disabled={!this.state.isNewData}
                        margin="normal"
                        variant="filled"
                        value={this.state.events}
                        onChange={(e) => this.setState({events : e.target.value})}
                        onBlur={(e) => this.updateKpiHeader('events', e.target.value)}
                        InputLabelProps={{
                            shrink: true,
                          }}
                        label="EVENT/OTHER ACTIVITY"
                        fullWidth />
                    <TextField multiline rowsMax="6"
                        disabled={!this.state.isNewData}
                        margin="normal"
                        variant="filled"
                        value={this.state.highlight}
                        onChange={(e) => this.setState({highlight : e.target.value})}
                        onBlur={(e) => this.updateKpiHeader('highlight', e.target.value)}
                        InputLabelProps={{
                            shrink: true,
                          }}
                        label="HIGHLIGHT"
                        fullWidth />
                    <TextField multiline rowsMax="6"
                        disabled={!this.state.isNewData}
                        margin="normal"
                        variant="filled"
                        value={this.state.nextactionplan}
                        onChange={(e) => this.setState({nextactionplan : e.target.value})}
                        onBlur={(e) => this.updateKpiHeader('nextactionplan', e.target.value)}
                        InputLabelProps={{
                            shrink: true,
                          }}
                        label="NEXT STEP/CORRECTION ACTION PLAN"
                        fullWidth />                      
                </Paper>
                <Dialog open={this.state.attachmentDialog} onClose={() => this.setState({attachmentDialog: false})}>
                {this.state.isUploading && <LinearProgress />}
                <form onSubmit={(ev) => this.handleUpload(ev)}>
                <DialogTitle>Lampiran: {this.state.uploadAttachmentDescription}</DialogTitle>
                <DialogContent style={{width: 500}}>
                    <input className="form-control"  ref={(ref) => { this.uploadInput = ref; }} type="file" />
                </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({attachmentDialog: false})} color="primary">
                            Cancel
                            </Button>
                            <Button type="submit" color="primary">
                                Upload
                            </Button>
                    </DialogActions>
                </form>
                </Dialog>
                <Dialog open={this.state.uncompleteDataDialog}
                    onClose={() => this.setState({uncompleteDataDialog: false})}>
                    <DialogTitle>Data Tidak Lengkap</DialogTitle>
                    <DialogContent>Mohon data dilengkapi terlebih dahulu sebelum submit laporan</DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({uncompleteDataDialog: false})}>
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={this.state.confirmationDialog}
                    onClose={() => this.setState({confirmationDialog: false})}>
                    <DialogTitle>Konfirmasi</DialogTitle>
                    <DialogContent>Apakah Anda yakin untuk mengirim laporan untuk bulan ini?</DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({confirmationDialog: false})}>
                        Cancel
                        </Button>
                        <Button onClick={() => this.handleSave()}>OK</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default withRouter(KpiInputScreen);