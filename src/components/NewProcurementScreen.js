import React from 'react';
import axios from 'axios';
import Downshift from 'downshift';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { FormControl, InputLabel, OutlinedInput, Select, MenuItem, Snackbar, Tooltip, DialogActions, DialogContent, TextField, LinearProgress, AppBar, Toolbar, IconButton, Tabs, Tab, Button, Paper, Table, TableHead, TableCell, TableRow, TableBody, Grid, Dialog, DialogTitle, Switch, TableFooter } from '@material-ui/core';
import { AttachFile, Book, History, ChevronRight, AddCircle, ChevronLeftRounded, ChevronLeft, SaveAlt, Launch, Delete, ListAlt, SearchOutlined, AddCircleOutline, CloudDownloadOutlined, DeleteOutline, CloudUpload } from '@material-ui/icons';
import MailIcon from '@material-ui/icons/Mail';

import { API_URL, BASE_URL } from '../config';

import BeritaAcaraScreen from './BeritaAcaraScreen';

export class NewProcurementScreen extends React.Component {
    state = {
        dialogOpen: false,
        editDialog: false,
        editIndex: null,
        backDialog: false,
        vendorDialog: false,
        commodityDialog: false,
        notYourAssignmentDialog: false,
        adminEvalDialog: false,
        technicalEvalDialog: false,
        commercialEvalDialog: false,
        attachmentDialog: false,
        deleteAttachmentDialog: false,
        workflowDialog: false,

        selectedCommodity: null,

        bidderList: [],
        bidderListError: false,
        buyerList: [],
        wfstatuslist: [],
        reportList: [],
        searchVendorList: [],
        selectCommodityList: [],
        adminEvalList: [],
        fungsiPenggunaList: [],
        milestone: [],
        attachmentList: [],

        wfhistoryId: null,
        wfhistoryLoading: false,
        wfDialog: false,
        wfHistoryList: [],

        rfqnum: '',
        docdate: null,
        buyername: '',
        prnum: '',
        title: '',
        proctype: '',
        rfqtype: 'JASA',
        procmethod: 'PILSUNG',
        prdate: null,
        bidopeningdate: null,
        bidopeningdate2: null,
        prebiddate: null,
        invitationdate: null,
        ambildokumendate: null,
        invtobidLetterNum: '',
        bidopeningLetterNum: '',
        adminEvalLetterNum: '',
        technicalEvalLetterNum: '',
        resultEvalLetterNum: '',
        awardLetterNum: '',
        currencycode: 'IDR',
        ownerestimate: 0,
        quoteprice: '',
        afternegoprice: '',
        sistemEvaluasiPenawaran: '',
        periode: '',
        justifikasimetode: '',
        sampul: '',
        tkdn: 0,
        fungsipengguna: '',
        estminvalue: null,
        estmaxvalue: null,
        adminevalRemarks: '',

        technicalEvalResult: false,
        technicalEvalRemarks: '',
        technicalEvalVendorName: '',
        commercialEvalResult: false,
        commercialEvalRemarks: '',
        commercialEvalVendorName: '',
        commercialWinner: false,

        wftype: null,
        wfstatus: '',
        wfapproval: null,

        tmpVendorid: '',
        tmpVendorname: '',
        tmpVendortype: '',
        tmpSktnum: '',
        tmpSanksi: '',
        tmpExpContractTitle: '',
        tmpExpCompanies: '',
        tmpRemarks: '',
        tmpGolusaha: '',

        isErrorVendorid: false,
        isErrorVendorname: false,
        isErrorVendortype: false,
        isErrorSktnum: false,
        isErrorSanksi: false,
        isErrorExpContractTitle: false,
        isErrorExpCompanies: false,
        isErrorRemarks: false,

        isErrorRfqnum: false,
        isErrorDocdate: false,
        isErrorPrnum: false,
        isErrorTitle: false,
        isErrorProctype: false,
        isErrorPrdate: false,
        isErrorDecision: false,
        isErrorBidopeningdate: false,
        isErrorPrebiddate: false,
        noAdminEval: false,

        isLoading: false,
        isDialogLoading: false,
        isNewDocument: true,
        showMessagebar: false,
        messagebar: '',
        rfqid: null,
        tabValue: 0,
        vendorid: '',

        decision: '',
        memo: '',
        reportServer: '',

        attachmentName: '',
        workflow: '',
        availableWorkflow: [],
    };

    handleUpload = (ev) => {
        ev.preventDefault();
        const { match: { params } } = this.props;

        this.setState({
            isErrorAttachmentName: false,
            isUploading: false,
        })

        if(this.state.attachmentName === '') {
            this.setState({
                isErrorAttachmentName: true
            })
            return;
        }

        const data = new FormData();
        data.append('file', this.uploadInput.files[0]);
        data.append('filename', this.state.attachmentName);
        data.append('rfqid', params.rfqid);

        let authToken = localStorage.getItem('authToken');
        this.setState({isUploading: true});
        axios.post(API_URL + '/uploadrfqfile', data, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        })
        .then((response) => {
            // reload daftar lampiran
            if(response.data.status === 'SUCCESS') {
                console.log(response.data);
                this.setState({
                    attachmentList: response.data.doclist,
                    attachmentDialog: false,
                    isUploading: false,
                })
            }
            
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    handleAttachmentDialog = () => {
        this.setState({
            tabValue: 5,
            attachmentDialog: true,
        })
    }

    handleCompleteAssignment = () => {
        this.setState({
            isErrorDecision: false,
            isUploading: true
        })

        if(this.state.decision === '') {
            this.setState({
                isErrorDecision: true,
                isUploading: false,
            })
            return;
        }

        // complete assignment
        let authToken = localStorage.getItem('authToken');
        var currentUser = localStorage.getItem('userEmail');
        currentUser = currentUser.split('@');
        axios.post(API_URL + '/completeassignment', {
            id: this.state.wfapproval.id,
            currentuser: currentUser[0],
            approvalstatus: this.state.decision,
            memo: this.state.memo
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isUploading: false,
                    messagebar: 'Success!',
                    showMessagebar: true,
                    approvalDialog: false,

                    // reload wfapproval
                    wfstatuslist: response.data.wfstatus,
                    wfapproval: response.data.wfapproval
                })
                console.log(this.state)
            }
            else {
                this.setState({
                    isUploading: false,
                    messagebar: 'Failed to start workflow',
                    showMessagebar: true,
                    approvalDialog: false
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Failed to start workflow',
                showMessagebar: true,
                approvalDialog: false
            })
        })
    }

    routeWorkflow = () => {
        let authToken = localStorage.getItem('authToken');
        if(this.state.rfqnum != '') {
            this.setState({
                isLoading: true,
                workflow: 'RFQPROCESS'
            })

            if(this.state.wftype == null) {
                // start workflow RFQPROCESS
                axios.post(API_URL + '/startworkflow', {
                    rfqid: this.state.rfqid,
                    wfname: this.state.workflow
                }, {
                    headers: {
                        Authorization: `Bearer ${authToken}`
                    }
                }).then((response) => {
                    if(response.data.status == 'SUCCESS') {
                        this.setState({
                            isLoading: false,
                            messagebar: 'Workflow started',
                            showMessagebar: true,
                            workflowDialog: false,
                            wfstatuslist: response.data.wfstatus,
                            wftype: this.state.workflow,
                            wfstatus: 'WAPPR',
                            wfapproval: response.data.wfapproval
                        })

                    }
                    else {
                        console.log(response)
                        this.setState({
                            isLoading: false,
                            messagebar: 'Failed to start workflow',
                            showMessagebar: true,
                        })
                    }
                    
                }).catch((error) => {
                    console.log(error)
                    this.setState({
                        isLoading: false,
                        messagebar: 'Failed to start workflow',
                        showMessagebar: true,
                    })
                })
            }
            else {
                // complete assignment
                // console.log(this.state)
                if(this.state.wfstatuslist != null && this.state.wfstatuslist[0].status == 'APPR' ) {
                    // complete
                    console.log('masup')
                    this.setState({
                        isLoading: false
                    })
                    return
                }
                let currentUser = localStorage.getItem('userEmail').split('@');
                if(currentUser[0] !== this.state.wfapproval.wfassignee.trim() && 1!=1) { //bypass if yang ini
                    // open not yours dialog
                    this.setState({
                        notYourAssignmentDialog: true,
                        isLoading: false
                    })
                }
                else {
                    // run approve dialog for you
                    this.setState({
                        approvalDialog: true,
                        isLoading: false,
                        decision: '',
                        memo: ''
                    })
                }
            }
        }
    }
    
    componentDidMount() {
        const { match: { params } } = this.props;

        let authToken = localStorage.getItem('authToken')
        axios.get(API_URL + `/getcommoditylistfromavailablevendor`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    selectCommodityList: response.data.commoditylist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })

        if(params.rfqid != null) {
            this.setState({isLoading: true})
            let authToken = localStorage.getItem('authToken')
            axios.get(API_URL + `/getprocurement/${params.rfqid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    let rfq = response.data.rfq
                    this.setState({
                        isLoading: false,
                        isNewDocument: false,

                        wftype: rfq.wftype,
                        wfstatus: rfq.wfstatus,
                        wfapproval: response.data.wfapproval,
                        attachmentList: rfq.doclist,

                        rfqnum: rfq.rfqnum,
                        title: rfq.title,
                        buyername: rfq.buyer.name,
                        rfqtype: rfq.rfqtype,
                        proctype: rfq.proctype,
                        procmethod: rfq.procmethod,
                        prnum: rfq.prnum,
                        docdate: rfq.documentdate,
                        prdate: rfq.prdate,
                        bidderList: rfq.rfqline,
                        rfqid: rfq.id,
                        bidopeningdate: rfq.bidopeningdate,
                        bidopeningdate2: rfq.bidopeningdate2,
                        invitationdate: rfq.invitationdate,
                        ambildokumendate: rfq.ambildokumendate,
                        prebiddate: rfq.prebiddate,
                        invtobidLetterNum: rfq.invitetobidletternum,
                        bidopeningLetterNum: rfq.bidopeningletternum,
                        adminEvalLetterNum: rfq.memoadminevalletternum,
                        technicalEvalLetterNum: rfq.technicalevalletternum,
                        resultEvalLetterNum: rfq.evalresultletternum,
                        awardLetterNum: rfq.awardletternum,
                        ownerestimate: rfq.ownerestimate,
                        currencycode: rfq.currencycode,
                        sistemEvaluasiPenawaran: rfq.sistemevaluasipenawaran,
                        periode: rfq.periode,
                        justifikasimetode: rfq.justifikasimetode,
                        sampul: rfq.sampul,
                        tkdn: rfq.tkdn,
                        fungsipengguna: rfq.fungsipengguna,
                        estminvalue: rfq.estminvalue,
                        estmaxvalue: rfq.estmaxvalue
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            // get workflow status
            axios.get(API_URL + `/getworkflowstatus/${params.rfqid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    let rfq = response.data.rfq
                    this.setState({
                        isLoading: false,
                        wfstatuslist: response.data.wfstatus
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            // get reports available
            axios.get(API_URL + `/getreports/${params.rfqid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        reportList: response.data.reports,
                        reportServer: response.data.reportserver
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            // get fungsi pengguna
            axios.get(API_URL + `/getfungsipengguna`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        fungsiPenggunaList: response.data.fungsipengguna
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            // get milestones
            axios.get(API_URL + `/getrfqmilestone/${params.rfqid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        milestone: response.data.milestone
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                })
            })

            // get available workflow
            axios.get(API_URL + `/getavailableworkflow/${params.rfqid}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        availableWorkflow: response.data.workflows
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                    })
                }
                
            }).catch((error) => {
                this.setState({
                    isLoading: false,
                })
            })
        }
        
    }

    updateKelengkapanDokumen(index) {
        var tmpBidderList = this.state.bidderList;
        var bidder = this.state.bidderList[index];
        
        this.setState({
            isLoading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/updatekelengkapandokumen`, {
            rfqlineid: bidder.id,
            kelengkapandokumen: !bidder.kelengkapandokumen,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                bidder.kelengkapandokumen = !bidder.kelengkapandokumen;
                tmpBidderList[index] = bidder;

                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    bidderList: tmpBidderList                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    updateAmbilDokumen(index) {
        var tmpBidderList = this.state.bidderList;
        var bidder = this.state.bidderList[index];
        
        this.setState({
            isLoading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/updateambildokumen`, {
            rfqlineid: bidder.id,
            ambildokumen: !bidder.ambildokumen,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                bidder.ambildokumen = !bidder.ambildokumen;
                tmpBidderList[index] = bidder;

                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    bidderList: tmpBidderList                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleDeleteAttachment = () => {
        this.setState({
            isUploading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/deleterfqattachment', {
            attachmentid: this.state.attachmentList[this.state.deletedAttachmentIndex].id,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    isUploading: false,
                    messagebar: 'Data berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    deleteAttachmentDialog: false,

                    attachmentList: response.data.attachmentlist,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Data tidak berhasil dihapus',
                    showMessagebar: true,
                    isNewDocument: false,
                    isUploading: false,
                    deleteAttachmentDialog: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Error: Data tidak berhasil dihapus',
                showMessagebar: true,
                isUploading: false,
                deleteDialog: false,
            })
        })
    }

    handleDelete(index) {
        var bidder = this.state.bidderList[index];
        this.setState({
            isLoading: true,
        })

        if(this.state.bidderList.length == 1) {
            // tinggal satu jadi ndak bisa dihapus
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Tidak bisa menghapus. Bidder harus lebih dari satu',
            })
            return;
        }

        let authToken = localStorage.getItem('authToken');
        if(bidder.id != null) {
            axios.get(API_URL + `/deleteproc/${bidder.id}`, {
                headers: {
                    Authorization: `Bearer ${authToken}`
                }
            }).then((response) => {
                if(response.data.status == 'SUCCESS') {
                    this.setState({
                        isLoading: false,
                        messagebar: 'Data berhasil dihapus',
                        showMessagebar: true,
                    })
                }
                else {
                    this.setState({
                        isLoading: false,
                        messagebar: 'Gagal menghapus data',
                        showMessagebar: true,
                    })
                }
                
            }).catch((error) => {
                console.log(error)
                this.setState({
                    isLoading: false,
                    messagebar: 'Gagal menghapus data',
                    showMessagebar: true,
                })
            })
        }

        // state delete
        var tmpbidderlist = this.state.bidderList;
        tmpbidderlist.splice(index, 1);
        this.setState({
            isLoading: false,
            bidderList: tmpbidderlist
        })
    }

    handleInputMilestone(timeside, index, value) {
        var tmpList = this.state.milestone
        if(timeside === 'startdate') {
            tmpList[index].startdate = value
        }
        else {
            tmpList[index].enddate = value
        }
        this.setState({
            milestone: tmpList
        })
    }

    handleSave = () => {
        var formError = false;
        this.setState({
            isErrorRfqnum: false,
            isErrorDocdate: false,
            isErrorPrnum: false,
            isErrorTitle: false,
            isErrorProctype: false,
            isErrorPrdate: false,
            bidderListError: false,
        })

        if(this.state.rfqnum === '') {
            this.setState({isErrorRfqnum: true})
            formError = true
        }
        if(this.state.prnum === '') {
            this.setState({isErrorPrnum: true})
            formError = true
        }
        if(this.state.title === '') {
            this.setState({isErrorTitle: true})
            formError = true
        }
        if(this.state.proctype === '') {
            this.setState({isErrorProctype: true})
            formError = true
        }
        if(this.state.docdate == null) {
            this.setState({isErrorDocdate: true})
            formError = true
        }
        if(this.state.prdate == null) {
            this.setState({isErrorPrdate: true})
            formError = true
        }
        if(this.state.bidderList.length === 0) {
            this.setState({bidderListError: true})
            formError = true
        }

        if(formError) {
            return;
        }

        // set post request
        let authToken = localStorage.getItem('authToken');
        let targetUrl = '';
        this.setState({isLoading: true})

        if(this.state.isNewDocument) {
            targetUrl = '/savenewprocurement';
        }
        else {
            targetUrl = '/updateprocurement';
        }

        // console.log(this.state.bidderList)

        axios.post(API_URL + targetUrl, {
            rfqnum: this.state.rfqnum,
            title: this.state.title,
            rfqtype: this.state.rfqtype,
            proctype: this.state.proctype,
            procmethod: this.state.procmethod,
            prnum: this.state.prnum,
            documentdate: this.state.docdate,
            prdate: this.state.prdate,
            bidderlist: this.state.bidderList,
            rfqid: this.state.rfqid,
            bidopeningdate: this.state.bidopeningdate,
            bidopeningdate2: this.state.bidopeningdate2,
            invitationdate: this.state.invitationdate,
            ambildokumendate: this.state.ambildokumendate,
            prebiddate: this.state.prebiddate,
            invitetobidletternum: this.state.invtobidLetterNum, 
            bidopeningletternum: this.state.bidopeningLetterNum,
            memoadminevalletternum: this.state.adminEvalLetterNum, 
            technicalevalletternum: this.state.technicalEvalLetterNum, 
            evalresultletternum: this.state.resultEvalLetterNum,
            awardletternum: this.state.awardLetterNum,
            ownerestimate: this.state.ownerestimate,
            currencycode: this.state.currencycode,
            sistemevaluasipenawaran: this.state.sistemEvaluasiPenawaran,
            periode: this.state.periode,
            justifikasimetode: this.state.justifikasimetode,
            sampul: this.state.sampul,
            tkdn: this.state.tkdn,
            fungsipengguna: this.state.fungsipengguna,
            estminvalue: this.state.estminvalue,
            estmaxvalue: this.state.estmaxvalue,
            milestone: this.state.milestone,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                    rfqid: response.data.rfq.id,
                    bidderList: response.data.rfqline,
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    messagebar: 'Dokumen tidak berhasil tersimpan',
                    showMessagebar: true,
                    isNewDocument: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                messagebar: 'Tidak dapat menyimpan dokumen',
                showMessagebar: true,
            })
        })
        

    }

    handleAddVendor = () => {
        // validate
        var formError = false;
        this.setState({
            isErrorVendorname: false,
            isErrorVendortype: false,
            isErrorSktnum: false,
            isErrorSanksi: false,
            isErrorExpContractTitle: false,
            isErrorExpCompanies: false,
            isErrorRemarks: false,
        })

        if(this.state.isErrorVendorid) {
            formError = true;
        }

        if(this.state.tmpVendorname === '') {
            this.setState({isErrorVendorname: true})
            formError = true;
        }

        if(this.state.tmpVendortype === '') {
            this.setState({isErrorVendortype: true})
            formError = true;
        }

        if(this.state.tmpSktnum === '') {
            this.setState({isErrorSktnum: true})
            formError = true;
        }

        if(this.state.tmpSanksi === '') {
            this.setState({isErrorSanksi: true})
            formError = true;
        }

        if(this.state.tmpExpContractTitle === '') {
            this.setState({isErrorExpContractTitle: true})
            formError = true;
        }

        if(this.state.tmpExpCompanies === '') {
            this.setState({isErrorExpCompanies: true})
            formError = true;
        }

        if(this.state.tmpRemarks === '') {
            this.setState({isErrorRemarks: true})
            formError = true;
        }

        if(formError) {
            return;
        }

        var tmpBidderList = this.state.bidderList;
        if(this.state.editDialog) {
            var bidder = tmpBidderList[this.state.editIndex]
            bidder.vendorid = this.state.tmpVendorid
            bidder.vendorname = this.state.tmpVendorname
            bidder.vendortype = this.state.tmpVendortype
            bidder.sktnum = this.state.tmpSktnum
            bidder.sanksi = this.state.tmpSanksi
            bidder.exp_contracttitle = this.state.tmpExpContractTitle
            bidder.exp_companies = this.state.tmpExpCompanies
            bidder.remarks = this.state.tmpRemarks
            bidder.golusaha = this.state.tmpGolusaha

            tmpBidderList[this.state.editIndex] = bidder;
        }
        else {
            var bidder = {
                vendorid: this.state.tmpVendorid,
                vendorname: this.state.tmpVendorname,
                vendortype: this.state.tmpVendortype,
                sktnum: this.state.tmpSktnum,
                sanksi: this.state.tmpSanksi,
                exp_contracttitle: this.state.tmpExpContractTitle,
                exp_companies: this.state.tmpExpCompanies,
                remarks: this.state.tmpRemarks,
                golusaha: this.state.tmpGolusaha,
            }
            tmpBidderList.push(bidder);
        }
       
        this.setState({bidderList: tmpBidderList, dialogOpen: false})
    }

    addBidderDialog = () => {
        this.setState({
            tmpVendorname: '',
            tmpVendortype: '',
            tmpSktnum: '',
            tmpSanksi: '',
            tmpExpContractTitle: '',
            tmpExpCompanies: '',
            tmpRemarks: '',
            tmpGolusaha: '',

            dialogOpen: true,
            editDialog: false,
        })
    }

    openHistoryDialog(wfid) {
        this.setState({
            wfDialog: true,
            wfhistoryLoading: true,
            wfHistoryList: []
        })

        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/getworkflowhistory/${wfid}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    wfhistoryLoading: false,
                    wfHistoryList: response.data.wfhistory,
                })
            }
            else {
                this.setState({
                    wfhistoryLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                wfhistoryLoading: false,
            })
        })
    }

    updateCommercialEval = () => {
        this.setState({
            isDialogLoading: true
        })

        let authToken = localStorage.getItem('authToken');
        const { match: { params } } = this.props;
        axios.post(API_URL + `/updatecommercialeval`, {
            rfqid: params.rfqid,
            vendorid: this.state.vendorid,
            result: this.state.commercialEvalResult,
            remarks: this.state.commercialEvalRemarks,
            winner: this.state.commercialWinner,
            quoteprice: this.state.quoteprice,
            afternegoprice: this.state.afternegoprice,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isDialogLoading: false,
                    commercialEvalDialog: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    updateTechnicalEval = () => {
        this.setState({
            isDialogLoading: true
        })

        // update ke backend
        let authToken = localStorage.getItem('authToken');
        const { match: { params } } = this.props;
        axios.post(API_URL + `/updatetechnicaleval`, {
            rfqid: params.rfqid,
            vendorid: this.state.vendorid,
            result: this.state.technicalEvalResult,
            remarks: this.state.technicalEvalRemarks,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isDialogLoading: false,
                    technicalEvalDialog: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleUpdateAdminEval = () => {
        this.setState({
            isDialogLoading: true
        })

        // update ke backend
        let authToken = localStorage.getItem('authToken');
        const { match: { params } } = this.props;
        axios.post(API_URL + `/updateadmineval`, {
            rfqid: params.rfqid,
            vendorid: this.state.vendorid,
            adminevallist: this.state.adminEvalList,
            recommendation: this.state.direkomendasikan,
            remarks: this.state.adminevalRemarks
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isDialogLoading: false,
                    adminEvalDialog: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })

    }

    handleAdminEvalNRSwitch(index) {
        var tempList = this.state.adminEvalList;
        // toggle
        var currentValue = tempList[index].evaladminresult;
        currentValue = (currentValue === 'NR') ? 'N/A' : 'NR';
        tempList[index].evaladminresult = currentValue;
        this.setState({
            adminEvalList: tempList
        });
    }

    handleAdminEvalSwitch(index) {
        var tempList = this.state.adminEvalList;
        // toggle
        var currentValue = tempList[index].evaladminresult;
        currentValue = (currentValue === 'ADA') ? 'N/A' : 'ADA';
        tempList[index].evaladminresult = currentValue;
        this.setState({
            adminEvalList: tempList
        });

    }

    handleCommercialEvalDialog(vendorid) {
        const { match: { params } } = this.props;
        this.setState({
            isDialogLoading: true,
            commercialEvalDialog: true,
            vendorid: vendorid,

            commercialEvalResult: false,
            commercialEvalRemarks: '',
            commercialWinner: false,
            quoteprice: '',
            afternegoprice: '',
        })

        // load from backend
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/getcommercialeval`, {
            rfqid: params.rfqid,
            vendorid: vendorid
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var evalz = response.data.eval
                if(evalz == null) {
                    this.setState({
                        noAdminEval: true,
                        isDialogLoading: false,
                    })
                }
                else {
                    this.setState({
                        noAdminEval: false,
                        isDialogLoading: false,
                        vendorid: vendorid,
                        commercialEvalResult: evalz.hasilkomersial == 'LULUS' ? true : false,
                        commercialEvalRemarks: evalz.keterangankomersial,
                        commercialWinner: evalz.winner,
                        technicalEvalVendorName: evalz.name,
                        quoteprice: evalz.quoteprice,
                        afternegoprice: evalz.afternegoprice,
                    })
                }
                
            }
            else {
                this.setState({
                    isDialogLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
            })
        })
    }

    handleTechnicalEvalDialog(vendorid) {
        const { match: { params } } = this.props;
        this.setState({
            isDialogLoading: true,
            technicalEvalDialog: true,
            vendorid: vendorid,

            technicalEvalResult: false,
            technicalEvalRemarks: ''
        })

        // load admin eval cek sudah lulus
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/gettechnicaleval`, {
            rfqid: params.rfqid,
            vendorid: vendorid
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var evalz = response.data.eval
                if(evalz == null) {
                    this.setState({
                        noAdminEval: true,
                        isDialogLoading: false,
                    })
                }
                else if(evalz.evaladminresult == '0') {
                    this.setState({
                        noAdminEval: true,
                        isDialogLoading: false,
                    })
                }
                else {
                    this.setState({
                        noAdminEval: false,
                        isDialogLoading: false,
                        vendorid: vendorid,
                        technicalEvalResult: evalz.hasil == 'LULUS' ? true : false,
                        technicalEvalRemarks: evalz.keterangan,
                        technicalEvalVendorName: evalz.name,
                    })
                }
                
            }
            else {
                this.setState({
                    isDialogLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
            })
        })
    }

    handleAdminEvalDialog(vendorid) {
        const { match: { params } } = this.props;
        this.setState({
            isDialogLoading: true,
            adminEvalDialog: true,
            vendorid: vendorid,
            adminevalRemarks: '',
        })

        // load admin eval list untuk vendor tsb dan rfq tsb
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/getadminevallist`, {
            rfqid: params.rfqid,
            vendorid: vendorid
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                console.log(response.data.recommendation)
                this.setState({
                    isDialogLoading: false,
                    adminEvalList: response.data.adminevallist,
                    direkomendasikan: (response.data.recommendation === '1') ? true : false,
                    adminevalRemarks: response.data.adminevalremarks,
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
            })
        })
    }

    openEditDialog(index) {
        var bidder = this.state.bidderList[index];
        this.setState({
            tmpVendorid: bidder.vendorid,
            tmpVendorname: bidder.vendorname,
            tmpVendortype: bidder.vendortype,
            tmpSktnum: bidder.sktnum,
            tmpSanksi: bidder.sanksi,
            tmpExpContractTitle: bidder.exp_contracttitle,
            tmpExpCompanies: bidder.exp_companies,
            tmpRemarks: bidder.remarks,
            tmpGolusaha: bidder.golusaha,

            dialogOpen: true,
            editDialog: true,
            editIndex: index,
        })

    }

    handleLoadVendorByCommodity = () => {
        this.setState({
            isUploading: true
        })

        let authToken = localStorage.getItem('authToken')
        axios.post(API_URL + `/loadvendorbycommodity`, {
            rfqid: this.state.rfqid,
            commid: this.state.selectedCommodity.commid,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                // refresh di sini...
                // console.log(response.data.bidderlist)
                this.setState({
                    bidderList: response.data.bidderlist,
                    isUploading: false,
                    commodityDialog: false
                })
            }
            else if(response.data.status === 'ERROR') {
                this.setState({
                    isUploading: false,
                    showMessagebar: true,
                    messagebar: response.data.message
                })
            }
        }).catch((error) => {
            console.log(error)
            this.setState({
                isUploading: false,
            })
        })
    }

    handleTabChange = (event, value) => {
        this.setState({tabValue: value})
    }

    handleSelectVendor(index) {
        let selected = this.state.searchVendorList[index];
        // console.log(selected)
        this.setState({
            tmpVendorid: selected.vendorid,
            tmpVendorname: selected.name,
            tmpVendortype: (selected.lettertype == null) ? 'NSKT' : 'VSKT',
            tmpSktnum: selected.aktapendirianno,
            tmpGolusaha: selected.golusaha,

            vendorDialog: false
        })
    }

    handleSearchVendor = (event) => {
        this.setState({
            vendorDialog: true,
            isDialogLoading: true,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/searchvendorbyname`, {
            term: event.target.value
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isDialogLoading: false,
                    searchVendorList: response.data.vendorlist,
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                wfhistoryLoading: false,
            })
        })
    }

    handleVendorIDChange = (event) => {
        this.setState({
            tmpVendorid: event.target.value,

            isErrorVendorid: false,
            isDialogLoading: true,

            tmpVendorname: '',
            tmpVendortype: '',
            tmpSktnum: '',
            tmpGolusaha: '',
        });

        // search vendor based on its code
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/getvendorbyid/${event.target.value}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            
            if(response.data.status == 'SUCCESS') {
                // console.log(response.data.vendor)
                this.setState({
                    isDialogLoading: false,

                    tmpVendorname: response.data.vendor.name,
                    isErrorVendorid: (response.data.vendor.name == null) ? true : false,
                    tmpVendortype: (response.data.vendor.aktapendirianno == null) ? 'NSKT' : 'VSKT',
                    tmpSktnum: response.data.vendor.aktapendirianno,
                    tmpGolusaha: response.data.vendor.golusaha
                })
            }
            else {
                this.setState({
                    isDialogLoading: false,
                    isErrorVendorid: true,
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isDialogLoading: false,
                isErrorVendorid: true,
            })
        })
    }

    render() {
        const { bidderList, bidderListError, showMessagebar, isNewDocument, tabValue } = this.state;
        const pageTitle = isNewDocument ? 'INPUT PENGADAAN BARU' : 'PROSES PENGADAAN';
        const profileName = localStorage.getItem('userName');
        const { match: { params } } = this.props;
        return (<div>
            {this.state.showMessagebar && (
                <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={showMessagebar}
                    onClose={() => this.setState({showMessagebar: false})}
                    autoHideDuration={6000}
                    message={<span>{this.state.messagebar}</span>}
                />
            )}

            <div style={{ height: 10, display: 'block' }} />
            
            <AppBar color="default" style={{marginTop: -10}} position="sticky">
                <Toolbar>
                    <Book />
                    <Typography style={{ flexGrow: 1 }} variant="body1">{pageTitle}</Typography>
                    <Tooltip title="Back">
                        <IconButton onClick={() => this.setState({backDialog: true})} color="primary">
                            <ChevronLeft />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Tambah Lampiran">
                        <IconButton disabled={isNewDocument} onClick={this.handleAttachmentDialog} color="primary">
                            <AttachFile />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Save">
                        <IconButton onClick={this.handleSave} color="primary">
                            <SaveAlt />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Route Approval">
                        <IconButton onClick={() => this.routeWorkflow()} disabled={isNewDocument} color="primary">
                            {(this.state.wftype != null && this.state.wfstatus != 'APPR') ? <Launch style={{color: 'green'}} /> : <Launch />}
                        </IconButton>
                    </Tooltip>
                    
                </Toolbar>
            </AppBar>
            {this.state.isLoading && <LinearProgress />}

            <Tabs value={tabValue} onChange={this.handleTabChange} >
                <Tab label="PENGADAAN" />
                <Tab disabled={isNewDocument} label="EVALUASI" />
                <Tab disabled={isNewDocument} label="BID DOCUMENT" />
                <Tab disabled={isNewDocument} label="ROUTING APPROVAL" />
                <Tab disabled={isNewDocument} label="REPORT" />
                <Tab disabled={isNewDocument} label="LAMPIRAN" />
            </Tabs>
            {tabValue === 0 && (
                <div>
                <Paper style={{padding: 20}}>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <TextField disabled={!isNewDocument} error={this.state.isErrorRfqnum} 
                                helperText={this.state.isErrorRfqnum && 'Isian tidak valid'} 
                                value={this.state.rfqnum} onChange={(e) => {this.setState({rfqnum: e.target.value})}} 
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="No. Pengadaan" 
                                required id="rfqnum" />

                            <TextField disabled={!isNewDocument} error={this.state.isErrorPrnum} 
                                helperText={this.state.isErrorPrnum && 'Isian tidak valid'} 
                                value={this.state.prnum} onChange={(e) => {this.setState({prnum: e.target.value})}} 
                                style={{marginTop: 12}} variant="outlined" 
                                fullWidth label="No. CRF/PR" required id="prnum" />

                            <TextField disabled style={{marginTop: 12}} variant="outlined" fullWidth 
                                label="Officer" 
                                value={isNewDocument ? profileName : this.state.buyername} />

                            <Grid container>
                                <Grid item xs={6} style={{paddingRight: 5}}>
                                    <TextField error={this.state.isErrorPrdate} 
                                        helperText={this.state.isErrorPrdate && 'Isian tidak valid'} 
                                        value={this.state.prdate} onChange={(e) => {this.setState({prdate: e.target.value})}} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal CRF/PR" required id="prdate" />  
                                                                   
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField error={this.state.isErrorDocdate} 
                                        helperText={this.state.isErrorDocdate && 'Isian tidak valid'} 
                                        value={this.state.docdate} onChange={(e) => {this.setState({docdate: e.target.value})}} 
                                        style={{marginTop: 12}} variant="outlined" 
                                        InputLabelProps={{shrink: true}} type="date" 
                                        fullWidth label="Tanggal Dokumen" required id="docdate" />                              
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item xs={6} style={{paddingRight: 5}}>
                                    <TextField value={this.state.invitationdate} 
                                        onChange={(e) => this.setState({invitationdate: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal Pengiriman Undangan" />
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField value={this.state.ambildokumendate} 
                                        onChange={(e) => this.setState({ambildokumendate: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal Pengambilan Dokumen" />
                                </Grid>
                                
                            </Grid>
                            <Grid container>
                                <Grid item xs={12}>
                                    <TextField error={this.state.isErrorPrebiddate} 
                                        helperText={this.state.isErrorPrebiddate && 'Isian tidak valid'} 
                                        value={this.state.prebiddate} onChange={(e) => this.setState({prebiddate: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal Prebid Meeting (Kosongkan Jika Tidak Perlu Prebid)" />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={6} style={{paddingRight: 5}}>
                                    <TextField error={this.state.isErrorBidopeningdate} 
                                        helperText={this.state.isErrorBidopeningdate && 'Isian tidak valid'} 
                                        value={this.state.bidopeningdate} 
                                        onChange={(e) => this.setState({bidopeningdate: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal Pembukaan Penawaran" />
                                </Grid>
                                <Grid item xs={6}>
                                <TextField  
                                        value={this.state.bidopeningdate2} 
                                        onChange={(e) => this.setState({bidopeningdate2: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        type="date" fullWidth label="Tanggal Pembukaan Penawaran Kedua" />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item xs={6} style={{paddingRight: 5}}>
                                    <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                        <InputLabel htmlFor="outlined-currencycode">Currency Code</InputLabel>
                                        <Select
                                            value={this.state.currencycode}
                                            onChange={(event) => {this.setState({currencycode: event.target.value})}}
                                            
                                            input={
                                            <OutlinedInput
                                                labelWidth={200}
                                                name="jenispengadaan"
                                                id="outlined-currencycode"
                                            />
                                            }
                                        >
                                            <MenuItem value="USD">USD</MenuItem>
                                            <MenuItem value="IDR">IDR</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField type="number" value={this.state.ownerestimate} 
                                        onChange={(e) => this.setState({ownerestimate: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} 
                                        fullWidth label="Owner Estimate (HPS/OE)" required/>
                                </Grid>
                            </Grid>
                            
                            
                            <TextField value={this.state.sistemEvaluasiPenawaran} onChange={(e) => this.setState({sistemEvaluasiPenawaran: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="Sistem Evaluasi Penawaran" required/>
                            <TextField multiline value={this.state.justifikasimetode} onChange={(e) => this.setState({justifikasimetode: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="Justifikasi Metode Pengadaan" required/>
                        </Grid>
                        <Grid item xs={6}>
                        
                            <TextField error={this.state.isErrorTitle} helperText={this.state.isErrorTitle && 'Isian tidak valid'} value={this.state.title} onChange={(e) => {this.setState({title: e.target.value})}} style={{marginTop: 12}} variant="outlined" fullWidth label="Judul Pengadaan" required id="title" />
                            <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-age-simple">Pengadaan Barang/Jasa</InputLabel>
                                <Select
                                    value={this.state.rfqtype}
                                    onChange={(event) => {this.setState({rfqtype: event.target.value})}}
                                    
                                    input={
                                    <OutlinedInput
                                        labelWidth={200}
                                        name="jenispengadaan"
                                        id="outlined-age-simple"
                                    />
                                    }
                                >
                                    <MenuItem value="BARANG">Barang</MenuItem>
                                    <MenuItem value="JASA">Jasa</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField error={this.state.isErrorProctype} helperText={this.state.isErrorProctype && 'Isian tidak valid'} value={this.state.proctype} onChange={(e) => {this.setState({proctype: e.target.value})}} style={{marginTop: 12}} variant="outlined" fullWidth label="Jenis Pengadaan" required id="proctype" />
                            <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-metode-pengadaan">Metode Pengadaan</InputLabel>
                                <Select
                                    value={this.state.procmethod}
                                    onChange={(event) => {this.setState({procmethod: event.target.value})}}
                                    input={
                                    <OutlinedInput
                                        labelWidth={200}
                                        name="metodepengadaan"
                                        id="outlined-metode-pengadaan"
                                    />
                                    }
                                >
                                    <MenuItem value="TUNSUNG">Penunjukan Langsung</MenuItem>
                                    <MenuItem value="PILSUNG">Pemilihan Langsung</MenuItem>
                                    <MenuItem value="LELANG">Pelelangan</MenuItem>
                                    <MenuItem value="SOURCING">Strategic Sourcing</MenuItem>
                                    <MenuItem value="CASHCARRY">Cash &amp; Carry</MenuItem>
                                    <MenuItem value="TURUNAN">Kontrak Turunan</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-metode-sampul">Metode Penyampaian Dokumen Penawaran</InputLabel>
                                <Select
                                    value={this.state.sampul}
                                    onChange={(event) => {this.setState({sampul: event.target.value})}}
                                    input={
                                    <OutlinedInput
                                        labelWidth={400}
                                        name="metodesampul"
                                        id="outlined-metode-sampul"
                                    />
                                    }
                                >
                                    <MenuItem value="1">1 Sampul</MenuItem>
                                    <MenuItem value="2">2 Sampul</MenuItem>
                                    <MenuItem value="3">2 Tahap</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField value={this.state.periode} onChange={(e) => this.setState({periode: e.target.value})} 
                                style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth 
                                label="Periode (Bulan)" />
                            <Grid container>
                                <Grid item style={{paddingRight: 5}} xs={6}>
                                    <TextField value={this.state.estminvalue} onChange={(e) => this.setState({estminvalue: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth 
                                        label="Estimasi Minimum Nilai Kontrak" />
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField value={this.state.estmaxvalue} onChange={(e) => this.setState({estmaxvalue: e.target.value})} 
                                        style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth 
                                        label="Estimasi Maksimum Nilai Kontrak" />
                                </Grid>
                            </Grid>
                            <TextField value={this.state.tkdn} onChange={(e) => this.setState({tkdn: e.target.value})} 
                                style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth 
                                label="Minimum TKDN (%)" />
                            <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-funsgi-pengguna">Fungsi Pengguna</InputLabel>
                                <Select
                                    value={this.state.fungsipengguna}
                                    onChange={(event) => {this.setState({fungsipengguna: event.target.value})}}
                                    input={
                                    <OutlinedInput
                                        labelWidth={400}
                                        name="metodesampul"
                                        id="outlined-funsgi-pengguna"
                                    />
                                    }
                                >
                                {this.state.fungsiPenggunaList.map((fungsi, index) => (
                                    <MenuItem value={fungsi.name}>{fungsi.description}</MenuItem>
                                ))}
                                </Select>
                            </FormControl>
                            
                        </Grid>
                    </Grid>
                    </Paper>
                    <Toolbar>
                        <Typography style={{ flexGrow: 1 }} variant="body1">BIDDER LIST</Typography>
                        
                        <Tooltip title="Tambah Bidder List">
                            <IconButton onClick={this.addBidderDialog} color="primary">
                                <AddCircle />
                            </IconButton>
                        </Tooltip>
                        <Tooltip title="Tambah dari Bidang/Subbidang">
                            <IconButton onClick={() => this.setState({commodityDialog: true})} color="primary">
                                <ListAlt />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                    <Paper>
                    {this.state.bidderListError && <div style={{color: 'red', fontSize: 13, padding: 20}}>Bidder List tidak boleh kosong</div>}
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nama</TableCell>
                                <TableCell>VSKT/NSKT</TableCell>
                                <TableCell>No. SKT</TableCell>
                                <TableCell>Gol Usaha</TableCell>
                                <TableCell>Sanksi</TableCell>
                                <TableCell>Pengalaman Judul Kontrak</TableCell>
                                <TableCell>Pengalaman Pengguna Barang/Jasa</TableCell>
                                <TableCell>Keterangan</TableCell>
                                <TableCell>&nbsp;</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {bidderList.map((bidder, index) => (
                                <TableRow key={index}>
                                    <TableCell><a style={{textDecoration: 'underline', cursor: 'pointer'}} onClick={() => {this.openEditDialog(index)}}>{bidder.vendorname}</a></TableCell>
                                    <TableCell>{bidder.vendortype}</TableCell>
                                    <TableCell>{bidder.sktnum}</TableCell>
                                    <TableCell>{bidder.golusaha}</TableCell>
                                    <TableCell>{bidder.sanksi}</TableCell>
                                    <TableCell>{bidder.exp_contracttitle}</TableCell>
                                    <TableCell>{bidder.exp_companies}</TableCell>
                                    <TableCell>{bidder.remarks}</TableCell>
                                    <TableCell>
                                    <IconButton onClick={() => {this.handleDelete(index)}} ><Delete /> </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                            {bidderList.length === 0 && (
                                <TableRow>
                                    <TableCell>Tidak ada data</TableCell>
                                </TableRow>
                            )}
                            
                        </TableBody>
                    </Table>
                </Paper>
                <Toolbar>
                    <Typography style={{ flexGrow: 1 }} variant="body1">JADWAL PELAKSANAAN PENGADAAN</Typography>
                </Toolbar>
                <Paper style={{padding: 20}}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Aktivitas</TableCell>
                                <TableCell>Mulai</TableCell>
                                <TableCell>Selesai</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.milestone.map((m, index) => (
                                <TableRow>
                                    <TableCell>{m.description}</TableCell>
                                    <TableCell>
                                        <TextField  
                                            value={m.startdate} 
                                            onChange={(e) => this.handleInputMilestone('startdate', index, e.target.value)}
                                            style={{marginTop: 12}} variant="outlined" 
                                            InputLabelProps={{shrink: true}} type="date" 
                                            fullWidth label="Mulai"  id="startdate" />
                                    </TableCell>
                                    <TableCell>
                                     <TextField  
                                            value={m.enddate} 
                                            onChange={(e) => this.handleInputMilestone('enddate', index, e.target.value)}
                                            style={{marginTop: 12}} variant="outlined" 
                                            InputLabelProps={{shrink: true}} type="date" 
                                            fullWidth label="Selesai"  id="enddate" />
                                    </TableCell>
                                </TableRow>
                            ))}
                            
                        </TableBody>
                    </Table>
                </Paper>
                <Toolbar>
                    <Typography style={{ flexGrow: 1 }} variant="body1">NOMOR SURAT-SURAT</Typography>
                </Toolbar>
                <Paper style={{padding: 20}}>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <TextField value={this.state.invtobidLetterNum} onChange={(e) => this.setState({invtobidLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Invitation to Bid" />
                            <TextField value={this.state.bidopeningLetterNum} onChange={(e) => this.setState({bidopeningLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Pembukaan Penawaran" />
                            <TextField value={this.state.adminEvalLetterNum} onChange={(e) => this.setState({adminEvalLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Memo Hasil Evaluasi Administrasi" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField value={this.state.technicalEvalLetterNum} onChange={(e) => this.setState({technicalEvalLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Pemberitahuan Hasil Evaluasi Teknis" />
                            <TextField value={this.state.resultEvalLetterNum} onChange={(e) => this.setState({resultEvalLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Pemberitahuan Hasil Evaluasi dan Penunjukan Pemenang" />
                            <TextField value={this.state.awardLetterNum} onChange={(e) => this.setState({awardLetterNum: e.target.value})} style={{marginTop: 12}} variant="outlined" InputLabelProps={{shrink: true}} fullWidth label="RefNum Penunjukan Pemenang" />
                        </Grid>
                    </Grid>
                </Paper>
                </div>
            )}
            {tabValue == 1 && (
                <div>
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama</TableCell>
                                    <TableCell>Mengambil Dokumen?</TableCell>
                                    <TableCell>Kelengkapan Dokumen <br/> Saat Pembukaan Penawaran</TableCell>
                                    <TableCell>Evaluasi Admin</TableCell>
                                    <TableCell>Evaluasi Teknis</TableCell>
                                    <TableCell>Evaluasi Komersial</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {bidderList.map((bidder, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{bidder.vendorname}</TableCell>
                                        <TableCell><Switch checked={bidder.ambildokumen} onChange={() => this.updateAmbilDokumen(index)} color="primary" /></TableCell>
                                        <TableCell><Switch checked={bidder.kelengkapandokumen} onChange={() => this.updateKelengkapanDokumen(index)} color="primary" /></TableCell>
                                        <TableCell><IconButton onClick={() => this.handleAdminEvalDialog(bidder.vendorid)}><AddCircle /></IconButton></TableCell>
                                        <TableCell><IconButton onClick={() => this.handleTechnicalEvalDialog(bidder.vendorid)}><AddCircle /></IconButton></TableCell>
                                        <TableCell><IconButton onClick={() => this.handleCommercialEvalDialog(bidder.vendorid)}><AddCircle /></IconButton></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            )}
            {tabValue == 2 && (
                <BeritaAcaraScreen rfqid={this.state.rfqid} rfqnum={this.state.rfqnum} />
            )}
            {tabValue == 3 && (
                <div>
                    <Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nama Approval</TableCell>
                                <TableCell>Status Approval</TableCell>
                                <TableCell>Current Assignment</TableCell>
                                <TableCell>Update Terakhir</TableCell>
                                <TableCell>Approval History</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.wfstatuslist.map((wf, index) => (
                                <TableRow key={index}>
                                    <TableCell>{wf.wfname}</TableCell>
                                    <TableCell>{wf.status}</TableCell>
                                    <TableCell>{wf.assignee}</TableCell>
                                    <TableCell>{wf.updated_at}</TableCell>
                                    <TableCell>
                                        <IconButton onClick={() => {this.openHistoryDialog(wf.id)}} ><History /> </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                            {this.state.wfstatuslist === 0 && (
                                <TableRow>
                                    <TableCell>Tidak ada data</TableCell>
                                </TableRow>
                            )}
                            
                        </TableBody>
                    </Table>
                    </Paper>
                </div>
            )}
            {tabValue == 4 && (
                <div><Paper>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nama Report</TableCell>
                                <TableCell>Download</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.reportList.map((report, index) => (
                                <TableRow>
                                    <TableCell>{report.reportname}</TableCell>
                                    <TableCell><a target="_blank" href={`${this.state.reportServer}/frameset?__report=${report.filename}&rfqnum=${params.rfqid}`}>Download</a></TableCell>
                                </TableRow>
                            ))}
                            {this.state.reportList === 0 && (
                                <TableRow>
                                    <TableCell>Tidak ada data</TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </Paper></div>
            )}
            {tabValue == 5 && (
                
                <Paper style={{padding: 20}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Nama Lampiran</TableCell>
                            <TableCell>File</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                     {this.state.attachmentList.map((attachment, index) => (
                            <TableRow>
                                <TableCell>{attachment.category}</TableCell>
                                <TableCell>{attachment.docname}</TableCell>
                                <TableCell>
                                    <div>
                                    <IconButton onClick={() => window.open(`${API_URL}/downloadrfqattachment/${attachment.id}`, '_blank')} ><CloudDownloadOutlined /></IconButton>
                                    <IconButton onClick={() => this.setState({deletedAttachmentIndex: index, deleteAttachmentDialog: true})} ><DeleteOutline /></IconButton>
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                </Paper>
            )}

                
                <Dialog open={this.state.dialogOpen} onClose={() => {this.setState({dialogOpen: false})}}>
                    {this.state.isDialogLoading && <LinearProgress />}
                    <DialogTitle>Input Bidder List</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <Grid container>
                            <Grid item xs={2}>
                                <TextField helperText={this.state.isErrorVendorid && 'Isian tidak valid'} error={this.state.isErrorVendorid} value={this.state.tmpVendorid} onChange={this.handleVendorIDChange} margin="dense" id="vendorid" label="Vendor ID" fullWidth autoFocus />
                            </Grid>
                            <Grid item xs={1}>
                                <IconButton onClick={() => this.setState({vendorDialog: true})}>
                                    <SearchOutlined />
                                </IconButton>
                            </Grid>
                        </Grid>
                        
                        <TextField disabled helperText={this.state.isErrorVendorname && 'Isian tidak valid'} error={this.state.isErrorVendorname} value={this.state.tmpVendorname} onChange={(e) => {this.setState({tmpVendorname: e.target.value})}} margin="dense" id="vendorname" label="Nama Perusahaan" fullWidth />
                        <TextField disabled helperText={this.state.isErrorVendortype && 'Isian tidak valid'} error={this.state.isErrorVendortype} value={this.state.tmpVendortype} onChange={(e) => {this.setState({tmpVendortype: e.target.value})}} margin="dense" id="vendortype" label="VSKT/NSKT" fullWidth />
                        <TextField disabled helperText={this.state.isErrorSktnum && 'Isian tidak valid'} error={this.state.isErrorSktnum} value={this.state.tmpSktnum} onChange={(e) => {this.setState({tmpSktnum: e.target.value})}} margin="dense" id="sktnum" label="No. SKT" fullWidth />
                        <TextField disabled value={this.state.tmpGolusaha} onChange={(e) => {this.setState({tmpGolusaha: e.target.value})}} margin="dense" id="golusaha" label="Golongan Usaha" fullWidth />
                        <TextField helperText={this.state.isErrorSanksi && 'Isian tidak valid'} error={this.state.isErrorSanksi} value={this.state.tmpSanksi} onChange={(e) => {this.setState({tmpSanksi: e.target.value})}} style={{marginBottom: 20}} margin="dense" id="sanksi" label="Sanksi" fullWidth />
                        <span style={{fontWeight: 'bold'}}>Pengalaman</span>
                        <TextField helperText={this.state.isErrorExpContractTitle && 'Isian tidak valid'} error={this.state.isErrorExpContractTitle} value={this.state.tmpExpContractTitle} onChange={(e) => {this.setState({tmpExpContractTitle: e.target.value})}} margin="dense" id="expcontracttitle" label="Judul Kontrak" fullWidth />
                        <TextField helperText={this.state.isErrorExpCompanies && 'Isian tidak valid'} error={this.state.isErrorExpCompanies} value={this.state.tmpExpCompanies} onChange={(e) => {this.setState({tmpExpCompanies: e.target.value})}} margin="dense" id="expcompanies" label="Nama Pengguna Barang/Jasa" fullWidth />
                        <TextField helperText={this.state.isErrorRemarks && 'Isian tidak valid'} error={this.state.isErrorRemarks} value={this.state.tmpRemarks} onChange={(e) => {this.setState({tmpRemarks: e.target.value})}} margin="dense" id="remarks" label="Keterangan" fullWidth />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({dialogOpen: false})} color="primary">
                        Batal
                        </Button>
                        <Button onClick={this.handleAddVendor} color="primary">
                        {this.state.editDialog ? 'Update' : 'Tambah'}
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.vendorDialog} onClose={() => this.setState({vendorDialog: false})}>
                    {this.state.isDialogLoading && <LinearProgress />}
                    <DialogTitle>Pencarian Vendor</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <TextField onChange={this.handleSearchVendor} fullWidth variant="outlined" label="Cari Vendor" style={{marginTop: 12}} InputLabelProps={{shrink: true}} />
                        <Paper>
                        {this.state.searchVendorList
                                .map((item, index) => (
                                <MenuItem onClick={() => {this.handleSelectVendor(index)}}>
                                    {item.vendorid} - {item.name}
                                </MenuItem>
                                ))}
                        </Paper>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({vendorDialog: false})} color="primary">
                        Close
                        </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.backDialog} onClose={() => this.setState({backDialog: false})}>
                    <DialogTitle>Konfirmasi</DialogTitle>
                    <DialogContent>
                        <DialogContent>Data di halaman ini akan hilang jika belum disimpan. Apakah yakin?</DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({backDialog: false})} color="primary">
                        Batal
                        </Button>
                        <Button component={Link} to={BASE_URL + "/app/escm"} color="primary">
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.wfDialog} onClose={() => this.setState({wfDialog: false})}>
                    {this.state.wfhistoryLoading && <LinearProgress />}
                    <DialogTitle>Approval History</DialogTitle>
                    <DialogContent>
                        <Table style={{tableLayout: 'auto'}}>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Tanggal</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Oleh</TableCell>
                                    <TableCell>Memo</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.wfHistoryList.map((wf, index) => (
                                    <TableRow>
                                        <TableCell style={{padding: 0}}>{wf.created}</TableCell>
                                        <TableCell>{wf.status}</TableCell>
                                        <TableCell>{wf.updatedby}</TableCell>
                                        <TableCell style={{padding: 0}}>{wf.memo}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({wfDialog: false})} color="primary">Close</Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.commodityDialog} onClose={() => this.setState({commodityDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Bidang/Subbidang</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <Downshift
                            onChange={selection => this.setState({selectedCommodity: selection})}
                            itemToString={item => (item ? item.description : '')}
                        >
                            {({
                            getInputProps,
                            getItemProps,
                            getLabelProps,
                            getMenuProps,
                            isOpen,
                            inputValue,
                            highlightedIndex,
                            selectedItem,
                            }) => (
                            <div>
                                <TextField fullWidth label="Cari Bidang/Subbidang" style={{marginTop: 12}} InputLabelProps={{shrink: true}} {...getInputProps()} />
                                <Paper {...getMenuProps()}>
                                {isOpen
                                    ? this.state.selectCommodityList
                                        .filter(item => item.description.includes(inputValue.toUpperCase()))
                                        .map((item, index) => (
                                        <MenuItem
                                            {...getItemProps({
                                            key: index,
                                            index,
                                            item,
                                            })}
                                        >
                                           {item.commid} - {item.description}
                                        </MenuItem>
                                        ))
                                    : null}
                                </Paper>
                            </div>
                            )}
                        </Downshift>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({commodityDialog: false})} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={this.handleLoadVendorByCommodity} color="primary">
                            Load
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.notYourAssignmentDialog} onClose={() => this.setState({notYourAssignmentDialog: false})}>
                    <DialogTitle>Bukan Tugas Anda</DialogTitle>
                    <DialogContent>
                        <DialogContent>Anda tidak memiliki assignment untuk workflow ini.</DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({notYourAssignmentDialog: false})} color="primary">
                        OK
                    </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.approvalDialog} onClose={() => this.setState({approvalDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Approval</DialogTitle>
                    <DialogContent>
                        <DialogContent>
                        <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-decision">Decision</InputLabel>
                                <Select
                                    value={this.state.decision}
                                    onChange={(event) => {this.setState({decision: event.target.value})}}
                                    error={this.state.isErrorDecision}
                                    helperText="Harus diisi"
                                    input={
                                    <OutlinedInput
                                        labelWidth={100}
                                        name="decision"
                                        id="outlined-decision"
                                    />
                                    }
                                >
                                    <MenuItem value="APPROVE">Approve</MenuItem>
                                    <MenuItem value="REVISE">Revise</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField value={this.state.memo} onChange={(ev) => this.setState({memo: ev.target.value})} fullWidth label="Memo" style={{marginTop: 12}} InputLabelProps={{shrink: true}} />
                        </DialogContent>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({approvalDialog: false})} color="primary">
                        Cancel
                        </Button>
                    <Button onClick={this.handleCompleteAssignment} color="primary">
                        OK
                    </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.adminEvalDialog} onClose={() => this.setState({adminEvalDialog: false})}>
                    {this.state.isDialogLoading && <LinearProgress />}
                    <DialogTitle>Evaluasi Administrasi</DialogTitle>
                    <DialogContent>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>No</TableCell>
                                        <TableCell>Deskripsi</TableCell>
                                        <TableCell>Required/Not Required</TableCell>
                                        <TableCell>Ada/Tidak Ada</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.adminEvalList.map((admineval, index) => (
                                        <TableRow>
                                            <TableCell>{admineval.name}</TableCell>
                                            <TableCell>{admineval.description}</TableCell>
                                            <TableCell>
                                                {admineval.name !== '8' && admineval.name !== '9' && (
                                                    <Switch onChange={() => this.handleAdminEvalNRSwitch(index)} color="primary" checked={admineval.evaladminresult !== 'NR'} />
                                                )}
                                            </TableCell>
                                            <TableCell>
                                                {admineval.name !== '8' && admineval.name !== '9' && (
                                                    <Switch disabled={admineval.evaladminresult === 'NR'} 
                                                        color="primary" onChange={() => this.handleAdminEvalSwitch(index)} 
                                                        checked={admineval.evaladminresult === 'ADA'} />
                                                )}
                                                
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TableCell colSpan={2}>Hasil Evaluasi (Direkomendasikan/Tidak Direkomendasikan)</TableCell>
                                        <TableCell>
                                            <Switch onChange={() => this.setState({direkomendasikan: !this.state.direkomendasikan})} checked={this.state.direkomendasikan} color="primary" />
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell colSpan={2}>Remarks</TableCell>
                                        <TableCell>
                                        <TextField onChange={(e) => this.setState({adminevalRemarks: e.target.value})} value={this.state.adminevalRemarks} fullWidth />
                                        </TableCell>
                                    </TableRow>
                                </TableFooter>
                            </Table>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({adminEvalDialog: false})} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleUpdateAdminEval} color="primary">
                        Save                    </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.technicalEvalDialog} onClose={() => this.setState({technicalEvalDialog: false})}>
                    {this.state.isDialogLoading && <LinearProgress />}
                    <DialogTitle>Evaluasi Teknis</DialogTitle>
                    <DialogContent>
                        <DialogContent>
                            {this.state.noAdminEval ? (
                                <div>Vendor tidak memenuhi persyaratan administrasi (tidak lulus atau belum diisi)</div>
                            ) : (
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Deskripsi</TableCell>
                                            <TableCell>Lulus/Tidak Lulus</TableCell>
                                            <TableCell>Keterangan</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>{this.state.technicalEvalVendorName}</TableCell>
                                            <TableCell>
                                                <Switch onChange={(e) => this.setState({technicalEvalResult: !this.state.technicalEvalResult})} checked={this.state.technicalEvalResult} color="primary" />
                                            </TableCell>
                                            <TableCell width={200}>
                                                <TextField onChange={(e) => this.setState({technicalEvalRemarks: e.target.value})} value={this.state.technicalEvalRemarks} fullWidth />
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                    
                                </Table>
                            )}
                            
                        </DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({technicalEvalDialog: false})} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.updateTechnicalEval} color="primary">
                        Save                    </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.commercialEvalDialog} onClose={() => this.setState({commercialEvalDialog: false})}>
                    {this.state.isDialogLoading && <LinearProgress />}
                    <DialogTitle>Evaluasi Komersial dan Penetapan Pemenang</DialogTitle>
                    <DialogContent>
                        <DialogContent>
                            {this.state.noAdminEval ? (
                                <div>Vendor tidak memenuhi persyaratan administrasi (tidak lulus atau belum diisi)</div>
                            ) : (
                                // <div>
                                //     <Typography variant="button">{this.state.technicalEvalVendorName}</Typography>
                                //     <Grid container>
                                //         <Grid item xs={6}>
                                //             <Typography variant="caption">Lulus/Tidak Lulus</Typography>
                                //         </Grid>
                                //         <Grid item xs={6}>
                                            
                                //         </Grid>
                                //     </Grid>
                                // </div>
                                
                                <Table>
                                    <TableBody>
                                        <TableRow>
                                            <TableCell>Nama Vendor:</TableCell>
                                            <TableCell>{this.state.technicalEvalVendorName}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Lulus/Tidak Lulus:</TableCell>
                                            <TableCell>
                                                <Switch onChange={(e) => this.setState({commercialEvalResult: !this.state.commercialEvalResult})} checked={this.state.commercialEvalResult} color="primary" />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Keterangan Peringkat:</TableCell>
                                            <TableCell>
                                                <TextField onChange={(e) => this.setState({commercialEvalRemarks: e.target.value})} value={this.state.commercialEvalRemarks} fullWidth />
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                    <TableFooter>
                                        <TableRow>
                                            <TableCell>Harga Penawaran (as bid)</TableCell>
                                            <TableCell colSpan={2}><TextField value={this.state.quoteprice} onChange={(e) => this.setState({quoteprice: e.target.value})} fullWidth /></TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Harga Setelah Negosiasi</TableCell>
                                            <TableCell colSpan={2}><TextField value={this.state.afternegoprice} onChange={(e) => this.setState({afternegoprice: e.target.value})} fullWidth /></TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell>Calon Pemenang?</TableCell>
                                            <TableCell colSpan={2}>
                                                <Switch onChange={(e) => this.setState({commercialWinner: !this.state.commercialWinner})} checked={this.state.commercialWinner} />
                                            </TableCell>
                                        </TableRow>
                                    </TableFooter>
                                </Table>
                            )}
                            
                        </DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({commercialEvalDialog: false})} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.updateCommercialEval} color="primary">
                        Save 
                    </Button>
                        
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.attachmentDialog} onClose={() => this.setState({attachmentDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <form onSubmit={this.handleUpload}>
                    <DialogTitle>Lampiran</DialogTitle>
                    <DialogContent style={{width: 500}}>
                        <TextField label="Nama Lampiran" 
                            error={this.state.isErrorAttachmentName}
                            helperText={this.state.isErrorAttachmentName && 'Nama lampiran harus diisi'}
                            value={this.state.attachmentName} 
                            fullWidth onChange={(e) => this.setState({attachmentName: e.target.value})} />
                        <input className="form-control"  ref={(ref) => { this.uploadInput = ref; }} type="file" />
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({attachmentDialog: false})} color="primary">
                        Cancel
                        </Button>
                        <Button type="submit" color="primary">
                            Upload
                        </Button>
                    </DialogActions>
                    </form>
                </Dialog>

                <Dialog open={this.state.deleteAttachmentDialog} onClose={() => this.setState({deleteAttachmentDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Konfirmasi</DialogTitle>
                    <DialogContent>
                        <DialogContent>Anda akan menghapus entri ini. Apakah yakin?</DialogContent>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({deleteAttachmentDialog: false})} color="primary">
                        Batal
                        </Button>
                        <Button onClick={this.handleDeleteAttachment} color="primary">
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.workflowDialog} onClose={() => this.setState({workflowDialog: false})}>
                    {this.state.isUploading && <LinearProgress />}
                    <DialogTitle>Workflow Approval</DialogTitle>
                        <DialogContent style={{width: 400}}>
                            <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                <InputLabel htmlFor="outlined-workflow">Workflow</InputLabel>
                                <Select
                                    value={this.state.workflow}
                                    onChange={(event) => {this.setState({workflow: event.target.value})}}
                                    input={
                                    <OutlinedInput
                                        labelWidth={100}
                                        name="workflow"
                                        id="outlined-workflow"
                                    />
                                    }
                                >
                                {!this.state.isLoading && this.state.availableWorkflow.map((workflow, index) => (
                                    <MenuItem value={workflow.description} key={index}>{workflow.description}</MenuItem>
                                ))}
                                    
                                </Select>
                            </FormControl>
                        </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.setState({workflowDialog: false})} color="primary">
                        Batal
                        </Button>
                        <Button onClick={this.routeWorkflow} color="primary">
                        OK
                        </Button>
                    </DialogActions>
                </Dialog>
        </div>);
    }
}

export default NewProcurementScreen;