import React from 'react';
import axios from 'axios';
import { LinearProgress, Snackbar, AppBar, Toolbar, Typography, Tabs, Tab, Paper, Tooltip, IconButton, Table, TableHead, TableRow, TableCell, TableBody, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, Grid, FormControl, InputLabel, Select, MenuItem, OutlinedInput } from '@material-ui/core';
import { MeetingRoom, AddCircle, Delete } from '@material-ui/icons';
import { API_URL, BASE_URL } from '../config';

export class MeetingRoomScreen extends React.Component {
    state = {
        isLoading: false,
        showMessagebar: false,
        messagebar: '',
        tabValue: 0,
        roomDialog: false,
        scheduleDialog: false,
        deleteDialog: false,
        isNewRoom: false,
        isNewSchedule: false,

        isNameError: false,
        isFloorError: false,
        isCapacityError: false,

        isScheduleNameError: false,
        isOrganizerError: false,
        isMeetingDateError: false,
        isStartTimeError: false,
        isFinishTimeError: false,
        isScheduleRoomIDError: false,
        isAttendanceError: false,

        meetingRooms: [],
        schedules: [],

        tmpRoomID: '',
        tmpName: '',
        tmpFloor: '',
        tmpCapacity: '',
        deletedIndex: '',

        tmpScheduleID: '',
        tmpScheduleName: '',
        tmpOrganizer: '',
        tmpMeetingDate: '',
        tmpStartTime: '',
        tmpFinishTime: '',
        tmpScheduleRoomID: '',
        tmpAttendance: '',
    }

    handleTabChange = (event, value) => {
        this.setState({tabValue: value})
    }

    addNewSchedule = () => {
        this.setState({
            isNewSchedule: true, 
            scheduleDialog: true,
            tmpScheduleName: '',
            tmpOrganizer: '',
            tmpMeetingDate: '',
            tmpStartTime: '',
            tmpFinishTime: '',
            tmpScheduleRoomID: '',
            tmpAttendance: ''
        })
    }

    handleScheduleDelete = () => {
        // delete yang di server
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/deleteschedule/${this.state.tmpScheduleID}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    scheduleDialog: false,
                    scheduleDeleteDialog: false,
                    schedules: response.data.schedules,
                    showMessagebar: true,
                    messagebar: 'Berhasil menghapus data'
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleDelete = () => {
        var tmpRoomList = this.state.meetingRooms
        let room = this.state.meetingRooms[this.state.deletedIndex]

        // delete yang di server
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/deleteroom/${room.id}`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                tmpRoomList.splice(this.state.deletedIndex, 1)
                this.setState({
                    isLoading: false,
                    deleteDialog: false,
                    meetingRooms: tmpRoomList,
                    showMessagebar: true,
                    messagebar: 'Berhasil menghapus data'
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    validateScheduleInput() {
        this.setState({
            isScheduleNameError: false,
            isOrganizerError: false,
            isMeetingDateError: false,
            isStartTimeError: false,
            isFinishTimeError: false,
            isScheduleRoomIDError: false,
            isAttendanceError: false,
        })
        if(this.state.tmpScheduleName == '') {
            this.setState({isScheduleNameError: true})
            return false
        }
        if(this.state.tmpOrganizer == '') {
            this.setState({isOrganizerError: true})
            return false
        }
        if(this.state.tmpMeetingDate == '') {
            this.setState({isMeetingDateError: true})
            return false
        }
        if(this.state.tmpStartTime == '') {
            this.setState({isStartTimeError: true})
            return false
        }
        if(this.state.tmpFinishTime == '') {
            this.setState({isFinishTimeError: true})
            return false
        }
        if(this.state.tmpStartTime === this.state.tmpFinishTime) {
            this.setState({isFinishTimeError: true})
            return false
        }
        if(this.state.tmpScheduleRoomID == '') {
            this.setState({isScheduleRoomIDError: true})
            return false
        }
        if(this.state.tmpAttendance == '') {
            this.setState({isAttendanceError: true})
            return false
        }
        return true
    }

    validateRoomInput() {
        this.setState({
            isNameError: false,
            isFloorError: false
        })
        if(this.state.tmpName == '') {
            this.setState({isNameError: true})
            return false
        }
        if(this.state.tmpFloor == '') {
            this.setState({isFloorError: true})
            return false
        }
        return true
    }

    handleUpdateSchedule = () => {
        if(!this.validateScheduleInput()) {
            return;
        }

        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/saveschedule`, {
            scheduleid: this.state.tmpScheduleID,
            schedulename: this.state.tmpScheduleName,
            organizer: this.state.tmpOrganizer,
            roomid: this.state.tmpScheduleRoomID,
            meetingdate: this.state.tmpMeetingDate,
            starttime: this.state.tmpStartTime,
            finishtime: this.state.tmpFinishTime,
            quantity: this.state.tmpAttendance
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var tmpSchedules = this.state.schedules 
                let newSchedule = response.data.schedule
                if(this.state.isNewSchedule) {
                    tmpSchedules.push(newSchedule)
                }
                else {
                    let curindex = tmpSchedules.findIndex(schedule => schedule.id === this.state.tmpScheduleID)
                    tmpSchedules[curindex] = newSchedule
                }
                
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    schedules: tmpSchedules,
                    scheduleDialog: false,                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    handleUpdateRoom = () => {
        if(!this.validateRoomInput()) {
            return;
        }

        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + `/saveroom`, {
            roomid: this.state.tmpRoomID, 
            name: this.state.tmpName,
            floor: this.state.tmpFloor,
            capacity: this.state.tmpCapacity,
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                var tmpRoomList = this.state.meetingRooms 
                let newRoom = response.data.room
                if(this.state.isNewRoom) {
                    tmpRoomList.push(newRoom)
                }
                else {
                    let curindex = tmpRoomList.findIndex(room => room.id === this.state.tmpRoomID)
                    tmpRoomList[curindex] = newRoom
                }
                
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Berhasil update data',
                    room: response.data.room,
                    meetingRooms: tmpRoomList,
                    roomDialog: false,                    
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal update data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
        
    }

    addNewRoom = () => {
        this.setState({
            isNewRoom: true, 
            roomDialog: true,
            tmpName: '',
            tmpFloor: '',
            tmpCapacity: '',
        })
    }

    openEditScheduleDialog(index) {
        this.setState({
            isNewSchedule: false, 
            scheduleDialog: true,
            tmpScheduleID: this.state.schedules[index].id,
            tmpScheduleName: this.state.schedules[index].name,
            tmpOrganizer: this.state.schedules[index].organizer,
            tmpScheduleRoomID: this.state.schedules[index].roomid,
            tmpMeetingDate: this.state.schedules[index].meetingdate,
            tmpStartTime: this.state.schedules[index].starttime,
            tmpFinishTime: this.state.schedules[index].finishtime,
            tmpAttendance: this.state.schedules[index].quantity
        })
    }

    openEditRoomDialog(index) {
        this.setState({
            isNewRoom: false, 
            roomDialog: true,
            tmpRoomID: this.state.meetingRooms[index].id,
            tmpName: this.state.meetingRooms[index].name,
            tmpFloor: this.state.meetingRooms[index].floor,
            tmpCapacity: this.state.meetingRooms[index].capacity,
        })
    }

    componentDidMount() {
        this.setState({isLoading: true});
        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + `/getmeetingroom`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status == 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    meetingRooms: response.data.meetingrooms,
                    schedules: response.data.schedules,                  
                })
            }
            else {
                this.setState({
                    isLoading: false,
                    showMessagebar: true,
                    messagebar: 'Gagal loading data',
                })
            }
            
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
                showMessagebar: true,
                messagebar: 'Gagal update data',
            })
        })
    }

    formatDate(meetingdate) {
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des']
        var meetdate = new Date(meetingdate)
        return meetdate.getDate() + ' ' + months[meetdate.getMonth()] + ' ' + meetdate.getFullYear()
    }

    render() {
        let times = [
            '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30',
            '11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30',
            '15:00', '15:30', '16:00',
        ];
        return(
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                {this.state.showMessagebar && (
                    <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={this.state.showMessagebar}
                        onClose={() => this.setState({showMessagebar: false})}
                        autoHideDuration={6000}
                        message={<span>{this.state.messagebar}</span>}
                    />
                )}
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <MeetingRoom /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">MEETING ROOM MANAGEMENT</Typography>
                        <Tooltip title="Tambah Jadwal Baru">
                            <IconButton onClick={this.addNewSchedule} color="primary">
                                <AddCircle />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                </AppBar>
                <Tabs value={this.state.tabValue} onChange={this.handleTabChange} >
                    <Tab label="DAFTAR KEGIATAN" />
                    <Tab label="MASTER DATA" />
                </Tabs>
                {this.state.tabValue === 0 && (
                    <Paper style={{padding: 20}}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Nama Kegiatan</TableCell>
                                    <TableCell>Organizer</TableCell>
                                    <TableCell>Tanggal</TableCell>
                                    <TableCell>Pukul</TableCell>
                                    <TableCell>Ruangan</TableCell>
                                    <TableCell>Peserta</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.schedules.map((schedule, index) => (
                                    <TableRow>
                                        <TableCell><a onClick={() => this.openEditScheduleDialog(index)} style={{textDecoration: 'underline', cursor: 'pointer'}}>{schedule.name}</a></TableCell>
                                        <TableCell>{schedule.organizer}</TableCell>
                                        <TableCell>{this.formatDate(schedule.meetingdate)}</TableCell>
                                        <TableCell>{schedule.starttime} - {schedule.finishtime}</TableCell>
                                        <TableCell>{schedule.room.name}</TableCell>
                                        <TableCell>{schedule.quantity}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                )}
                {this.state.tabValue === 1 && (
                    <div>
                        <Toolbar>
                        <Typography style={{flexGrow: 1}} variant="body1">DATA RUANGAN</Typography>
                        <Tooltip title="Tambah Ruangan">
                            <IconButton onClick={this.addNewRoom} color="primary">
                                <AddCircle />
                            </IconButton>
                        </Tooltip>
                    </Toolbar>
                    <Paper>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>NAMA RUANGAN</TableCell>
                                    <TableCell>LANTAI</TableCell>
                                    <TableCell>Kapasitas</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.meetingRooms.map((mr, index) => (
                                    <TableRow key={index}>
                                        <TableCell><a onClick={() => this.openEditRoomDialog(index)} style={{textDecoration: 'underline', cursor: 'pointer'}}>{mr.name}</a></TableCell>
                                        <TableCell>{mr.floor}</TableCell>
                                        <TableCell>{mr.capacity}</TableCell>
                                        <TableCell>
                                        <IconButton onClick={() => this.setState({deleteDialog: true, deletedIndex: index})}><Delete /></IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </Paper>
                    </div>
                )}
                <Dialog open={this.state.roomDialog} onClose={() => this.setState({roomDialog: false})}>
                    <DialogTitle>{this.state.isNewRoom ? 'Tambah Ruangan' : 'Edit Ruangan'}</DialogTitle>
                    <DialogContent>
                        <TextField 
                            error={this.state.isNameError}
                            helperText={this.state.isNameError && "Nama Ruangan harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Nama Ruangan"
                            style={{marginTop: 20}}
                            value={this.state.tmpName}
                            onChange={(e) => this.setState({tmpName: e.target.value})} />
                        <TextField 
                            error={this.state.isFloorError}
                            helperText={this.state.isFloorError && "Lantai Ruangan harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Lantai Ruangan"
                            style={{marginTop: 20}}
                            value={this.state.tmpFloor}
                            onChange={(e) => this.setState({tmpFloor: e.target.value})} />
                        <TextField 
                            error={this.state.isCapacityError}
                            helperText={this.state.isCapacityError && "Lantai Ruangan harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Kapasitas"
                            style={{marginTop: 20}}
                            value={this.state.tmpCapacity}
                            onChange={(e) => this.setState({tmpCapacity: e.target.value})} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({roomDialog: false})} color="primary">Cancel</Button>
                        <Button onClick={this.handleUpdateRoom} color="primary">{this.state.isNewRoom ? 'Add' : 'Update'}</Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={this.state.scheduleDialog} onClose={() => this.setState({scheduleDialog: false})}>
                    <DialogTitle>{this.state.isNewSchedule ? 'Tambah Jadwal' : 'Edit Jadwal'}</DialogTitle>
                    <DialogContent>
                        <TextField 
                            error={this.state.isScheduleNameError}
                            helperText={this.state.isScheduleNameError && "Nama Kegiatan harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Nama Kegiatan"
                            style={{marginTop: 20}}
                            value={this.state.tmpScheduleName}
                            onChange={(e) => this.setState({tmpScheduleName: e.target.value})} />
                        <TextField 
                            error={this.state.isOrganizerError}
                            helperText={this.state.isOrganizerError && "Organizer harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Organizer"
                            style={{marginTop: 20}}
                            value={this.state.tmpOrganizer}
                            onChange={(e) => this.setState({tmpOrganizer: e.target.value})} />

                        <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                            <InputLabel required htmlFor="ruangan">Ruangan</InputLabel>
                            <Select 
                                error={this.state.isScheduleRoomIDError}
                                helperText={this.state.isScheduleRoomIDError && "Ruangan harus diisi"}
                                value={this.state.tmpScheduleRoomID}
                                onChange={(e) => this.setState({tmpScheduleRoomID: e.target.value})}
                                input={
                                    <OutlinedInput labelWidth={100} name="ruangan"
                                        id="ruangan" />
                                }
                            >
                            {this.state.meetingRooms.map((room, index) => (
                                <MenuItem value={room.id} key={index}>{room.name}</MenuItem>
                            ))}
                            </Select>
                        </FormControl>
                        <TextField 
                            type="date"
                            InputLabelProps={{shrink: true}}
                            error={this.state.isMeetingDateError}
                            helperText={this.state.isMeetingDateError && "Tanggal harus diisi"}
                            variant="outlined"
                            fullWidth
                            required
                            label="Tanggal"
                            style={{marginTop: 20}}
                            value={this.state.tmpMeetingDate}
                            onChange={(e) => this.setState({tmpMeetingDate: e.target.value})} />
                        <Grid container spacing={24}>
                            <Grid item xs={6}>
                                <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                    <InputLabel required htmlFor="mulai">Mulai</InputLabel>
                                    <Select 
                                        error={this.state.isStartTimeError}
                                        helperText={this.state.isStartTimeError && "Jam Mulai harus diisi"}
                                        value={this.state.tmpStartTime}
                                        onChange={(e) => this.setState({tmpStartTime: e.target.value})}
                                        input={
                                            <OutlinedInput labelWidth={100} name="mulai"
                                                id="mulai" />
                                        }
                                    >
                                    {times.map((time, index) => (
                                        <MenuItem value={time} key={index}>{time}</MenuItem>
                                    ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth style={{marginTop: 12}} variant="outlined">
                                    <InputLabel required htmlFor="selesai">Selesai</InputLabel>
                                    <Select 
                                        error={this.state.isFinishTimeError}
                                        helperText={this.state.isFinishTimeError && "Jam Selesai harus diisi"}
                                        value={this.state.tmpFinishTime}
                                        onChange={(e) => this.setState({tmpFinishTime: e.target.value})}
                                        input={
                                            <OutlinedInput labelWidth={100} name="selesai"
                                                id="selesai" />
                                        }
                                    >
                                    {times.map((time, index) => (
                                        <MenuItem value={time} key={index}>{time}</MenuItem>
                                    ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            
                        </Grid>
                        <TextField 
                                error={this.state.isAttendanceError}
                                helperText={this.state.isAttendanceError && "Jumlah Peserta harus diisi"}
                                variant="outlined"
                                fullWidth
                                required
                                label="Jumlah Peserta"
                                style={{marginTop: 20}}
                                value={this.state.tmpAttendance}
                                onChange={(e) => this.setState({tmpAttendance: e.target.value})} />
                        
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({scheduleDeleteDialog: true})} color="secondary">Delete</Button>
                        <div style={{flexGrow: 1}}>&nbsp;</div>
                        <Button onClick={() => this.setState({scheduleDialog: false})} color="primary">Cancel</Button>
                        <Button onClick={this.handleUpdateSchedule} color="primary">{this.state.isNewSchedule ? 'Add' : 'Update'}</Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={this.state.deleteDialog} onClose={() => this.setState({deleteDialog: false})}>
                    <DialogTitle>Konfirmasi Penghapusan</DialogTitle>
                    <DialogContent>Anda yakin akan menghapus data ini?</DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({deleteDialog: false})} color="primary">Cancel</Button>
                        <Button onClick={this.handleDelete} color="primary">CONFIRM</Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={this.state.scheduleDeleteDialog} onClose={() => this.setState({scheduleDeleteDialog: false})}>
                    <DialogTitle>Konfirmasi Penghapusan</DialogTitle>
                    <DialogContent>Anda yakin akan menghapus data ini?</DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({scheduleDeleteDialog: false})} color="primary">Cancel</Button>
                        <Button onClick={this.handleScheduleDelete} color="primary">CONFIRM</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}