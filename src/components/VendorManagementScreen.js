import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import {
    AppBar, Toolbar, Paper, TextField, Button, Table, TableHead, TableRow, TableCell, TableBody, TableFooter,
    TablePagination, FormGroup, FormControlLabel, Switch, Menu, MenuItem, MenuList, OutlinedInput, FormControl, InputLabel, Select,
    LinearProgress, IconButton, Tabs, Tab, Grid
} from '@material-ui/core';
import { LocalShipping, ChevronRight, AddCircle, KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import { FirstPageIcon } from '@material-ui/icons/FirstPage';
import { LastPageIcon } from '@material-ui/icons/LastPage';

import { API_URL, BASE_URL } from '../config';

export class VendorManagementScreen extends React.Component {
    state = {
        isLoading: false,
        vendorList: [],
        vendorCommodityList: [],
        originalVendorList: [],
        isCheckedVerifiedVendor: false,

        // table pagination
        rowsPerPage: 20,
        page: 0,
        tabValue: 0,
        commodity: '',
        golusaha: 'ALL',
    }

    componentWillMount() {
        // cek authorization
        let authorizations = localStorage.getItem('userAuth');
        if (!authorizations.includes('VENDORMANAGEMENT')) {
            this.props.history.push(BASE_URL + '/');
        }
    }

    filterVerifiedVendor = () => {
        let filterverified = (this.state.isCheckedVerifiedVendor) ? 'ALL' : 'VERIFIED';
        this.setState({
            isLoading: true,
            isCheckedVerifiedVendor: !this.state.isCheckedVerifiedVendor,
        })

        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/filtervendor', {
            verified: filterverified
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if (response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    vendorList: response.data.vendorlist,
                    originalVendorList: response.data.vendorlist
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }

    componentDidMount() {
        this.setState({
            isLoading: true,
        });

        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + '/vendorlist', {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if (response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    vendorList: response.data.vendorlist,
                    originalVendorList: response.data.vendorlist
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }

    handleSearchbyCommodity = (e) => {
        this.setState({
            isLoading: true
        })
        let authToken = localStorage.getItem('authToken');
        axios.post(API_URL + '/vendorbycommodity', {
            bidangusaha: this.state.commodity.toUpperCase(),
            golusaha: this.state.golusaha
        }, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if (response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    vendorCommodityList: response.data.vendorlist,
                    originalVendorList: response.data.vendorlist
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }

    handleSearch = (e) => {
        if (e.target.value.length < 3)
            return;

        var tblist = this.state.originalVendorList.filter((row) => {
            return row.name.toUpperCase().indexOf(e.target.value.toUpperCase()) >= 0
        })

        this.setState({
            vendorList: tblist
        })
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    render() {
        const { rowsPerPage, page, vendorList, vendorCommodityList, tabValue } = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, vendorList.length - page * rowsPerPage);
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{ marginTop: -10 }} position="static">
                    <Toolbar>
                        <LocalShipping /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">VENDOR MANAGEMENT</Typography>
                        <Button color="primary" component={Link} to={BASE_URL + "/app/newvendor"} variant="contained" aria-label="Input Baru">
                            <AddCircle />&nbsp;
                            VENDOR BARU
                        </Button>
                    </Toolbar>
                </AppBar>
                <Tabs value={tabValue} onChange={(ev, value) => { this.setState({ tabValue: value }) }}>
                    <Tab label="Nama Perusahaan" />
                    <Tab label="Bidang Usaha" />
                </Tabs>
                {tabValue === 0 && (
                    <div>
                        <Paper style={{ padding: 10 }}>
                            <TextField
                                id="outlined-full-width"
                                label="Nama Perusahaan"
                                placeholder="Ketik kata kunci di sini"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                onInput={this.handleSearch}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            {/* <Button color="primary" variant="contained">Search</Button> */}
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Switch color="primary" checked={this.state.isCheckedVerifiedVendor} onChange={this.filterVerifiedVendor} />
                                    }
                                    label="Verified Vendor"
                                />
                            </FormGroup>
                        </Paper>
                        <br />
                        <Paper>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Vendor ID</TableCell>
                                        <TableCell>Nama</TableCell>
                                        <TableCell>Owner</TableCell>
                                        <TableCell>Detail</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.vendorList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((vendor, index) => (
                                        <TableRow key={index}>
                                            <TableCell>{vendor.sapid}</TableCell>
                                            <TableCell>{vendor.name}</TableCell>
                                            <TableCell>{vendor.ownerid}</TableCell>
                                            <TableCell><IconButton component={Link} to={`${BASE_URL}/app/vendordetail/${vendor.id}`}><ChevronRight /></IconButton></TableCell>
                                        </TableRow>
                                    ))}
                                    {this.state.vendorList.length === 0 && (
                                        <TableRow>
                                            <TableCell>Tidak ada data</TableCell>
                                        </TableRow>
                                    )}

                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            rowsPerPageOptions={[5, 10, 25]}
                                            colSpan={4}
                                            count={vendorList.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            SelectProps={{
                                                native: true,
                                            }}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        // ActionsComponent={TablePaginationActionsWrapped}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </Paper>
                    </div>
                )}
                {tabValue === 1 && (
                    <div>
                        <Paper style={{ padding: 10 }}>
                            <Grid container spacing={24}>
                                <Grid item xs={6}>
                                    <TextField
                                        label="Nama Bidang Usaha"
                                        placeholder="Ketik kata kunci di sini"
                                        fullWidth
                                        value={this.state.commodity}
                                        margin="normal"
                                        variant="outlined"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={(e) => this.setState({ commodity: e.target.value })}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth style={{ marginTop: 15 }} variant="outlined">
                                        <InputLabel htmlFor="outlined-golusaha">Golongan Usaha</InputLabel>
                                        <Select
                                            value={this.state.golusaha}
                                            onChange={(event) => { this.setState({ golusaha: event.target.value }) }}
                                            input={
                                                <OutlinedInput
                                                    labelWidth={100}
                                                    name="golusaha"
                                                    id="outlined-golusaha"
                                                    shrink={true}
                                                />
                                            }
                                        >
                                            <MenuItem key={0} value="ALL">ALL</MenuItem>
                                            <MenuItem key={1} value="B1">B1</MenuItem>
                                            <MenuItem key={2} value="B2">B2</MenuItem>
                                            <MenuItem key={3} value="Besar">Besar</MenuItem>
                                            <MenuItem key={4} value="M1">M1</MenuItem>
                                            <MenuItem key={5} value="M2">M2</MenuItem>
                                            <MenuItem key={6} value="Menengah">Menengah</MenuItem>
                                            <MenuItem key={7} value="K1">K1</MenuItem>
                                            <MenuItem key={8} value="K2">K2</MenuItem>
                                            <MenuItem key={9} value="K3">K3</MenuItem>
                                            <MenuItem key={10} value="Kecil">Kecil</MenuItem>
                                            <MenuItem key={11} value="N/A">N/A</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                            <Button onClick={this.handleSearchbyCommodity} color="primary" variant="contained">
                                CARI
                                    </Button>
                        </Paper>
                        <br />
                        <Paper>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Vendor ID</TableCell>
                                        <TableCell>Nama</TableCell>
                                        <TableCell>Bidang Usaha</TableCell>
                                        <TableCell>Gol Usaha</TableCell>
                                        <TableCell>Detail</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.vendorCommodityList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((vendor, index) => (
                                        <TableRow key={index}>
                                            <TableCell>{vendor.vendorid}</TableCell>
                                            <TableCell>{vendor.name}</TableCell>
                                            <TableCell>{vendor.bidangusaha}</TableCell>
                                            <TableCell>{vendor.golusaha}</TableCell>
                                            <TableCell><IconButton component={Link} to={`${BASE_URL}/app/vendordetail/${vendor.id}`}><ChevronRight /></IconButton></TableCell>
                                        </TableRow>
                                    ))}
                                    {this.state.vendorCommodityList.length === 0 && (
                                        <TableRow>
                                            <TableCell>Tidak ada data</TableCell>
                                        </TableRow>
                                    )}

                                </TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TablePagination
                                            rowsPerPageOptions={[5, 10, 25]}
                                            colSpan={5}
                                            count={vendorCommodityList.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            SelectProps={{
                                                native: true,
                                            }}
                                            onChangePage={this.handleChangePage}
                                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        // ActionsComponent={TablePaginationActionsWrapped}
                                        />
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </Paper>
                    </div>
                )}
            </div>
        )
    }
}

class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

export default VendorManagementScreen;