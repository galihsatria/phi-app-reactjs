import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { LinearProgress, AppBar, Toolbar, IconButton, Tabs, Tab, Button, Paper, Table, TableHead, TableCell, TableRow, TableBody } from '@material-ui/core';
import { LocalShipping, ChevronRight, AddCircle, Book } from '@material-ui/icons';
import MailIcon from '@material-ui/icons/Mail';

import { API_URL, BASE_URL } from '../config';

export class BidderListScreen extends React.Component {
    state = {
        rfqlist: [],
        isLoading: false,
    };

    componentWillMount() {
        // cek authorization
        let authorizations = localStorage.getItem('userAuth');
        if(!authorizations.includes('ESCM')) {
            this.props.history.push(BASE_URL + '/');
        }
    }
    
    componentDidMount() {
        
        this.setState({isLoading: true})

        let authToken = localStorage.getItem('authToken');
        axios.get(API_URL + '/getprocurementlist', {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status = 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    rfqlist: response.data.rfqlist
                })
            }
        }).catch((error) => {
            console.log(error);
            this.setState({
                isLoading: false,
            })
        })
    }

    render() {
        
        return (<div>
            {this.state.isLoading && <LinearProgress />}
            {/* <Typography variant="title">Vendor Management</Typography>
            <Typography variant="subtitle1">Bidang, Subbidang, Prakualifikasi</Typography> */}

            <div style={{ height: 10, display: 'block' }} />
           
            
            <AppBar color="default" style={{marginTop: -10}} position="static">
                <Toolbar>
                    <Book /> &nbsp;
                    <Typography style={{ flexGrow: 1 }} variant="button">SCM ELECTRONIC PROCESS</Typography>
                    <Button color="primary" component={Link} to={BASE_URL + "/app/inputpengadaan"} variant="contained" aria-label="Input Baru">
                        <AddCircle />&nbsp;
                        PENGADAAN BARU
                    </Button>
                </Toolbar>
            </AppBar>

            
            
            <Paper>
                <TableHead>
                    <TableRow>
                        <TableCell>No Pengadaan</TableCell>
                        <TableCell>Judul Pengadaan</TableCell>
                        <TableCell>No CRF/PR</TableCell>
                        <TableCell>Tanggal CRF/PR</TableCell>
                        <TableCell>Tanggal Dokumen</TableCell>
                        <TableCell>Officer</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.rfqlist.map((rfq, index) => (
                        <TableRow>
                            <TableCell>{rfq.rfqnum}</TableCell>
                            <TableCell>{rfq.title}</TableCell>
                            <TableCell>{rfq.prnum}</TableCell>
                            <TableCell>{rfq.prdate}</TableCell>
                            <TableCell>{rfq.documentdate}</TableCell>
                            <TableCell>{rfq.buyer}</TableCell>
                            <TableCell><IconButton component={Link}  to={`${BASE_URL}/app/inputpengadaan/${rfq.id}`}><ChevronRight /></IconButton></TableCell>
                        </TableRow>
                    ))}
                    
                </TableBody>
            </Paper>
        </div>);
    }
}

export default BidderListScreen;
