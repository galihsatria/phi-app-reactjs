import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { AppBar, Toolbar, Paper, TextField, Button, Table, TableHead, TableRow, TableCell, TableBody, TableFooter, 
    TablePagination, FormControl, InputLabel, Select, OutlinedInput, MenuItem, FormControlLabel, Switch,
    LinearProgress, IconButton, Dialog, DialogTitle, DialogContent, DialogActions, Grid } from '@material-ui/core';
import { LocalShipping, ChevronRight, AddCircle, KeyboardArrowRight, KeyboardArrowLeft, TrendingUp, TagFaces, DetailsOutlined, DetailsRounded } from '@material-ui/icons';

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import { API_URL, BASE_URL } from '../config';

export class KpiScreen extends React.Component {
    state = {
        isLoading: false,
        dialog: false,
        showMessagebar: false,
        messagebar: '',
        periodlist: [],
        period: '',
        groupAp: '',

        chartOptions: [],
    }

    componentWillMount() {
        this.setState({isLoading: true})
        let authToken = localStorage.getItem('authToken')
        axios.get(API_URL + `/kpidashboard`, {
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        }).then((response) => {
            if(response.data.status === 'SUCCESS') {
                this.setState({
                    isLoading: false,
                    periodlist: response.data.periods,
                    chartOptions: response.data.chartoptions,
                    groupAp: response.data.groupap,
                })
                console.log(response.data.chartoptions)
            }
            else {
                this.setState({isLoading: false})
            }
        }).catch((error) => {
            console.log(error)
            this.setState({
                isLoading: false,
            })
        })
    }
    
    render() {
        return (
            <div>
                {this.state.isLoading && <LinearProgress />}
                <div style={{ height: 10, display: 'block' }} />
                <AppBar color="default" style={{marginTop: -10}} position="static">
                    <Toolbar>
                    <TrendingUp /> &nbsp;
                        <Typography style={{ flexGrow: 1 }} variant="button">SCM KPI - {this.state.groupAp}</Typography>
                        <Button component={Link} to={BASE_URL + "/app/kpidetail"} color="primary" variant="contained" aria-label="Detail Laporan">
                            <ChevronRight />
                            DETAIL LAPORAN
                        </Button>
                        &nbsp;
                        <Button onClick={() => this.setState({dialog: true})} color="primary" variant="contained" aria-label="Input Baru">
                            <AddCircle />&nbsp;
                            SUBMIT LAPORAN BULANAN
                        </Button>
                    </Toolbar>
                </AppBar>
                <br/>
                    <Grid container spacing={24}>
                    {this.state.chartOptions.map((chart, index) => (
                        <Grid key={index} item xs={4}>
                            <HighchartsReact
                            highcharts={Highcharts}
                            options={chart}
                            />

                            {chart.currentIndicator && (
                            <div style={{backgroundColor: 'white', paddingVertical: 20}}>
                                <Grid container spacing={10}>
                                    <Grid item xs={4}>
                                        <center>
                                            <img width={30} height={30} src={`../img/${chart.oneyearIndicator}.jpg`} />
                                            <Typography style={{fontSize: 12, color: 'grey'}} variant="button">12 MRA</Typography>
                                        </center>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <center>
                                        <img width={30} height={30} src={`../img/${chart.ytdIndicator}.jpg`} />
                                            <Typography style={{fontSize: 12, color: 'grey'}} variant="button">YTD</Typography>
                                        </center>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <center>
                                            <img width={30} height={30} src={`../img/${chart.currentIndicator}.jpg`} />
                                            <Typography style={{fontSize: 12, color: 'grey'}} variant="button">Current Month</Typography>
                                        </center>
                                    </Grid>
                                </Grid>
                                <br/> &nbsp;
                            </div>
                            )}
                        </Grid>
                    ))}
                        
                    </Grid>
                    

                <Dialog open={this.state.dialog}
                    onClose={() => this.setState({dialog: false})}>
                    <DialogTitle>Periode Laporan</DialogTitle>
                    <DialogContent>
                        <FormControl fullWidth style={{marginTop: 12, marginBottom: 12}} variant="outlined">
                            <InputLabel htmlFor="outlined-period">Periode</InputLabel>
                            <Select
                                value={this.state.period}
                                onChange={(event) => {this.setState({period: event.target.value})}}
                                input={
                                <OutlinedInput
                                    labelWidth={200}
                                    name="period"
                                    id="outlined-period"
                                />
                                }
                            >
                                {this.state.periodlist.map((period, index) => (
                                    <MenuItem key={index} value={period}>{period}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.setState({dialog: false})}>
                            Cancel
                        </Button>
                        <Button disabled={this.state.period == ''} component={Link} to={BASE_URL + "/app/kpiinput/" + this.state.period}>
                            OK
                        </Button>
                    </DialogActions>
                </Dialog>
                
            </div>
        )
    }
}

export default KpiScreen;