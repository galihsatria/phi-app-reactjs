import React, { Component } from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import './App.css';
import AuthSwitchScreen from './components/AuthSwitchScreen';
import ResponsiveDrawer from './components/ResponsiveDrawer';

class App extends Component {
  state = {
    authToken: null,
  }

  componentDidMount() {
    
    // console.log('Token: ' + localStorage.getItem('authToken'));
  }

  render() {
    return (
      <div className="App">
        <Router>
          <AuthSwitchScreen />
        </Router>
      </div>
    );
  }
}

export default App;
